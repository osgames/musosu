/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudokuGenerator;

import sudoku.Sudoku;
import sudokuSolver.Solver;

public abstract class GenerateMethod {
	protected Sudoku game;
	protected int desiredDifficulty;
	private String name;
	protected Solver solver;
	
	public static final int VERY_EASY = 20;
	public static final int EASY = 40;
	public static final int MEDIUM = 60;
	public static final int HARD = 80;
	public static final int VERY_HARD = 100;
	
	public GenerateMethod(String name, int desiredDifficulty){
		this.name = name;
		this.desiredDifficulty = desiredDifficulty;
		game = new Sudoku();
		game.setFileName("None");
		solver = new Solver();
	}
	
	public abstract Sudoku generate();
	
	protected int taxify(){
		double di = game.getDifficulty();
		di *= 10;
		int dif = (int) di;
		return dif;
	}
	
	public String getName(){ 
		return name;
	}
	
	protected int getDifficulty(){
		solver.setGame(game);
		return (int)(solver.estimateDifficulty() * 10);
	}
}
