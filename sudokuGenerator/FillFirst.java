/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudokuGenerator;

import java.util.Date;
import java.util.Random;
import java.util.Vector;

import sudoku.Sudoku;
import sudokuSolver.SolvedSquare;
import sudokuSolver.Solver;

public class FillFirst extends GenerateMethod{
	private Random rand;
	
	public FillFirst(int difficulty){
		super("Fill First", difficulty);
	}
	
	public Sudoku generate(){
		boolean check = false;
		int num;
		
		rand = new Random();
		// date is used as seed for the random generator
		Date date = new Date(); 
		rand.setSeed(date.getTime());
		// create empty sudoku
		while(!check){
			game = new Sudoku();
			// fill all cells with random numbers respecting sudoku rules
			randomFill();
			// now random remove
			check = randomRemove();
		}
		for(int i = 0; i < 81; i++){
			num = game.get(i);
			if(num != 0)
				game.set(num, i);
		}
		return game;	
	}

	private boolean randomRemove(){
		int pos, cd, tpos, tval, tcd, tcount = 0;
		Solver solver = new Solver();
		
		for(int i = 0; i < 25; i++){
			pos = rand.nextInt(81);
			while(game.get(pos) == 0)
				pos = rand.nextInt(81);
			game.remove(pos);
		}
		
		solver.setGame(game);
		cd = (int) (10 * (solver.estimateDifficulty()));
		
		while(Math.abs(cd - this.desiredDifficulty) > 5){
			pos = rand.nextInt(81);
			while(game.get(pos) == 0)
				pos = rand.nextInt(81);
			tpos = pos;
			tval = game.get(tpos);
			game.remove(pos);
			solver.setGame(game);
			tcd = cd;
			cd = (int) (10 * (solver.estimateDifficulty()));
			if(cd > 100){
				game.add(tval, tpos);
				cd = tcd;
				tcount++;
				if(tcount > 20) // avoid loop lock
					return false;
			}
			if(cd > 100) return false;
		}
		return true;
	}
	
	private void randomFill(){
		int i, pos, val;
		
		// randomly fill first  box - TODO more scientific randomness...
		for(i = 0; i < 9; i++){
			pos = rand.nextInt(9);
			while(game.get(pos) != 0)
				pos = rand.nextInt(9);
			val = rand.nextInt(9) + 1;
			while(!game.canIAdd(val, pos))
				val = rand.nextInt(9) + 1;
			game.add(val, pos);
		}
		game.setFileName("as");
		solver.setGame(game);
		Vector <SolvedSquare> sqv = solver.runMethodStandalone(6);
		game.addFromSQV(sqv);
	}
}
