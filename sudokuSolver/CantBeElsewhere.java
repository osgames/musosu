/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudokuSolver;

import sudoku.Convert;
import sudoku.Sudoku;

/**
 * <p><code>CantBeElsewhere</code> is a simple Solving method which places a
 * value in a square, if there is no other possible square where it could
 * be put.</p>
 * @author Visvardis Marios
 * @since version 0.8.4
 */
public class CantBeElsewhere extends SolveMethod{
	public CantBeElsewhere(Sudoku game){
		super("Can't be elsewhere", SolveMethod.MODERATE, game);
	}
	
	public int passOnce(){
		int count; int contrib = 0;
		int index, mode;
		int position = -1;
		
		for(int i = 0; i < 9; i++){
			for(mode = 0; mode < 3; mode++){
				for(int num = 1; num < 10; num++){
					count = 0;
					for(int j = 0; j < 9; j++){
						index = Convert.translateToIndex(mode, i, j);
						if(game.getPossibles(index).contains(num)){
							count++;
							position = index;
						}
					}
					if(count == 1){
						add(position, num);
						contrib++;
					}
				}
			}
		}
		contribution += contrib;
		return contrib;
	}
}
