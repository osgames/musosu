/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudokuSolver;

import java.util.Vector;

public class SolvedSquare {
	private int index;
	private int value;
	private Vector<SolveAction> actions;
	private boolean interactive;
	
	public SolvedSquare(int index, int value){
		this.index = index;
		this.value = value;
		this.interactive = false;
	}
	
	public SolvedSquare(int index, int value, String methodName, Vector<SolveAction> actions){
		this.index = index;
		this.value = value;
		this.actions = actions;
		this.interactive = true;
	}
	
	public boolean isInteractive(){return interactive;}
	public int getValue(){return value;}
	public int getIndex(){return index;}
	public Vector<SolveAction> getActions(){return actions;}
}
