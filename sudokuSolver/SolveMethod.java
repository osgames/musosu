/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudokuSolver;

import java.util.Vector;

import sudoku.Convert;
import sudoku.Sudoku;

public abstract class SolveMethod {
	protected Vector<SolvedSquare> solvedSquares;
	private String name;
	protected Sudoku game;
	private int type;
	protected int contribution;
	private long passTime;
	private long startTime;
	private long totalTime;
	
	public static final int ELIMINATION = 1;
	public static final int MODERATE = 2;
	public static final int ADVANCED = 3;
	public static final int EVIL = 4;
	public static final int BRUTE_FORCE = 5;
	
	public SolveMethod(String name, int type, Sudoku game){
		this.name = name;
		this.type = type;	
		this.game = game;
		this.contribution = 0;
		this.totalTime = 0;
		solvedSquares = new Vector<SolvedSquare>();
	}
	
	public abstract int passOnce();	
	
	public Vector<SolvedSquare> getSolvedSquares(){
		if(solvedSquares.size() == 0)
			return null;
		else
			return solvedSquares;
	}
	
	public String getName(){return name;}
	public int getType(){return type;}
	
	public double getTotalTimeSecs(){
		double time;
		time = Double.valueOf(String.valueOf(totalTime)) / 1000.0;
		return time;
	}
	
	public double getPassTimeSecs(){
		double time;
		time = Double.valueOf(String.valueOf(passTime)) / 1000.0;
		return time;
	}
	
	public long getPassTimeMillis(){
		return passTime;
	}
	
	public long getTotalTimeMillis(){
		return totalTime;
	}
	
	public void startTimer(){
		startTime = System.currentTimeMillis();
	}
	
	public void stopTimer(){
		passTime = System.currentTimeMillis() - startTime;
		totalTime += passTime;
	}
	
	protected void add(int x, int y, int value){
		SolvedSquare sq = new SolvedSquare(Convert.kartesToIndex(x, y), value);
		solvedSquares.add(sq);
	}
	
	protected void add(int index, int value){
		SolvedSquare sq = new SolvedSquare(index, value);
		solvedSquares.add(sq);
//		System.out.println("Added value " + value + " at cell " + index);
	}
	
	protected void remove(int index){
		int i;
		for(i = 0; i < solvedSquares.size(); i++){
			if(solvedSquares.get(i).getIndex() == index){
				solvedSquares.remove(i);
				break;
			}
		}
	}
	
	public int getMethodContribution(){
		return contribution;
	}
	
	public String getTypeDefinition(int cat){
		String txt;
		switch(cat){
		case SolveMethod.ELIMINATION:
			txt = "Simple elimination Methods";
			break;
		case SolveMethod.MODERATE:
			txt = "Moderate Methods";
			break;
		case SolveMethod.ADVANCED:
			txt = "Advanced Methods";
			break;
		case SolveMethod.EVIL:
			txt = "Evil Methods";
			break;
		case SolveMethod.BRUTE_FORCE:
			txt = "Trial & Error Methods";
			break;
		default:
			txt = null;
			break;
		}
		return txt;
	}
}
