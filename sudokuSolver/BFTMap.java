/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudokuSolver;

import java.util.Vector;

public class BFTMap{
	private Vector<Trial> trials;
	
	public BFTMap(){
		trials = new Vector<Trial>();
	}
	
	public void addRecord(int position, int trialIndex){
		trials.add(new Trial(position, trialIndex));
	}
	
	public int stepBack(){
		int curr, pos;
		int size = trials.size();
		int i = size - 1;
		// removing last node records
		curr = trials.get(i).getPosition();
		pos = trials.get(i).getPosition();
		while(pos == curr){
			trials.remove(i);
			i--;
			if(i >= 0)
				pos = trials.get(i).getPosition();
			else return -1;
		}
		// check if the func arg is invalid - no such trial
		// return the possibles index on defined position
		if(i < 0)
			return -1; // error code
		else
			return pos;	
	}
	
	public int getLastIndex(){
		if(trials.size() == 0) return -1;
		int i = trials.size() - 1;
		
		return trials.get(i).getTrialIndex();	
	}
	
	public int getLastAddPosition(){
//		if(trials.isEmpty())
//			return -1;
		return trials.get(trials.size() - 1).getPosition();
	}
	
	class Trial{
		private int position;
		private int trialIndex;
		
		public Trial(int position, int trialIndex){
			this.position = position;
			this.trialIndex = trialIndex;
		}
		
		public int getPosition(){ return this.position; }
		public int getTrialIndex(){ return this.trialIndex; }
	}
}
