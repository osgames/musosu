/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudokuSolver;

import sudoku.Convert;
import sudoku.Sudoku;

public class EliminationA extends SolveMethod{
	public EliminationA(Sudoku game){
		super("Row-Column-Box Elimination", SolveMethod.ELIMINATION, game);
	}
	
	public int passOnce(){
		this.startTimer();
		int contrib = 0;
		int row, col, pR = 0, pC = 0, pB, num, r, x, y, cpos, count;
		int[] pRpB; int[] kart; int[] pCpB;
		solvedSquares.clear();
	
		for(num = 1; num < 10; num++){
			if(game.getNumberInstances(num) != 9){
				// For Rows
				for(row = 0; row < 9; row += 3){
					if((pRpB = possibleRB(num, row)) != null){
						pR = pRpB[0]; pB = pRpB[1];
						kart = Convert.koutiaToKartes(pB, 0);
						y = kart[1];
						cpos = 0;
						for(count = 0; count < 3; count++){
							r = game.get(Convert.kartesToIndex(pR, y));
							if(r == 0){
								if(game.existsInColumn(num, y) == -1){
									pC = y;
									cpos++;
								}
							}
							y++;
						}
						if(cpos == 1){
							add(pR, pC, num);
							contrib++;
						}
					}
				}
				
				// For Columns
				for(col = 0; col < 9; col += 3){
					if((pCpB = possibleCB(num, col)) != null){
						pC = pCpB[0]; pB = pCpB[1];
						kart = Convert.koutiaToKartes(pB, 0);
						x = kart[0];
						cpos = 0;
						for(count = 0; count < 3; count++){
							r = game.get(Convert.kartesToIndex(x, pC));
							if(r == 0){
								if(game.existsInLine(num, x) == -1){
									pR = x;
									cpos++;
								}
							}
							x++;
						}
						if(cpos == 1){
							add(pR, pC, num);
							contrib++;
						}
					}
				}			
			}
		}		
		contribution = contribution + contrib;
		stopTimer();
		return contrib;
	}
	
	private int[] possibleRB(int number, int startLine){
		int[] ret = new int[2];
		int possibleLine = -1, possibleBox = -1;
		int count, cpos, canBeHere = 0;
		
		cpos = 0;
		for(count = 0; count < 3; count++){
			if((game.existsInLine(number, startLine + count) == -1) 
				|| game.isLineFull(startLine + count)){
				cpos++;
				canBeHere = startLine + count;
			}
		}
		
		if(cpos == 1){
			for(int c:Convert.getBoxesOnLine(canBeHere)){
				if(game.existsInBox(number, c) == -1){
					possibleBox = c;
					possibleLine = canBeHere;
				}
			}
			ret[0] = possibleLine; ret[1] = possibleBox;
			return ret;
		}else{
			return null;
		}
	}
	
	private int[] possibleCB(int number, int startColumn){
		int count, cpos, canBeHere = 0;
		int[] ret = new int[2];
		int possibleColumn = -1, possibleBox = -1;
		
		cpos = 0;
		for(count = 0; count < 3; count++){
			if((game.existsInColumn(number, startColumn + count) == -1) 
				|| game.isColumnFull(startColumn + count)){
				cpos++;
				canBeHere = startColumn + count;
			}
		}
		
		if(cpos == 1){
			for(int c: Convert.getBoxesOnColumn(canBeHere)){
				if(game.existsInBox(number, c) == -1){
					possibleBox = c;
					possibleColumn = canBeHere;
				}
			}
			ret[0] = possibleColumn; ret[1] = possibleBox;
			return ret;
		}else{
			return null;
		}
	}
}
