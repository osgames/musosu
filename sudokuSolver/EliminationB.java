/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudokuSolver;

import java.util.Vector;

import sudoku.Sudoku;

public class EliminationB extends SolveMethod{
	public EliminationB(Sudoku game){
		super("Only possible value", SolveMethod.ELIMINATION, game);
	}
	
	public int passOnce(){
		startTimer();
		Vector<Integer> possibles;
		solvedSquares.clear();
		int contrib = 0;
		for(int i = 0; i < 81; i++){
			if((possibles = game.getPossibles(i)).size() == 1 && game.get(i) == 0){
				add(i, possibles.get(0));
				contrib++;
			}
		}
		contribution = contribution + contrib;
		stopTimer();
		return contrib;
	}
}
