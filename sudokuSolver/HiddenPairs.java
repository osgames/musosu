/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudokuSolver;

import java.util.Vector;

import sudoku.Convert;
import sudoku.Sudoku;

/**
 * <p><code>HiddenPairs</code> is an implementation of the wide-spread
 * sudoku solving method. 
 * @author Visvardis Marios
 * @since version 0.8.4
 */
public class HiddenPairs extends SolveMethod{
	private Vector<HPair> hiddenPairs;
	
	public HiddenPairs(Sudoku game){
		super("Hidden Pairs", SolveMethod.ADVANCED, game);
		hiddenPairs = new Vector<HPair>();
	}
	
	public int passOnce(){
		startTimer();
		int contrib = 0, position = -1, count, mode, index;

		solvedSquares.clear();
		game.refreshPossibleMap();
		if(excludePossibles() != 0){
			for(int i = 0; i < 9; i++){
				for(mode = 0; mode < 3; mode++){
					for(int num = 1; num < 10; num++){
						count = 0;
						for(int j = 0; j < 9; j++){
							index = Convert.translateToIndex(mode, i, j);
							if(game.getPossibles(index).contains(num)){
								count++;
								position = index;
							}
						}
						if(count == 1){
							add(position, num);
							contrib++;
						}
					}
				}
			}	
		}
		contribution += contrib;
		stopTimer();
		return contrib;
	}
	
	public int findHiddenPairs(){
		int where11 = -1, where12 = -1, num1 = 0, num2 = 0;
		int count, countp = 0, found = 0, i, j, num, mode;
		int w1 = -1, w2 = -1;
		HPair hp;
		
		for(i = 0; i < 9; i++){
			for(mode = 0; mode < 3; mode++){
				countp = 0;
				for(num = 1; num < 10; num++){
					count = 0;
					for(j = 0; j < 9; j++){
						if(game.getPossibles(Convert.translateToIndex(mode, i, j)).contains(new Integer(num))){
							if(count == 0) w1 = j;
							if(count == 1) w2 = j;
							count++;
						}
					}
					if(count == 2){
						if(countp == 0){
							where11 = w1;
							where12 = w2;
							num1 = num;
						}else if(countp == 1){
							num2 = num;
							if(where11 == w1 && where12 == w2){
								hp = new HPair(mode, num1, num2, Convert.translateToIndex(mode, i, where11), Convert.translateToIndex(mode, i, where12));
								hiddenPairs.add(hp);
								found++;
							}
						}
						countp++;
					}
				}
			}
		}
		return found;
	}
	
	private int excludePossibles(){
		int found = 0;
		int a, b, w1, w2;
		hiddenPairs.clear();
		
		findHiddenPairs();
		
		if(hiddenPairs.size() == 0)
			return 0;
		
		for(HPair hp: hiddenPairs){
			found++;
			a = hp.getNum1();
			b = hp.getNum2();
			w1 = hp.getWhere1();
			w2 = hp.getWhere2();
			
			for(int num = 1; num < 10; num++){
				if(num != a && num != b){
					game.setImpossible(num, w1);
					game.setImpossible(num, w2);
				}
			}
		}
		
		return found;
	}	
}



class HPair{
	private int num1;
	private int num2;
	private int where1;
	private int where2;
	private int mode;
	
	public HPair(int mode, int num1, int num2, int where1, int where2){
		this.num1 = num1;
		this.num2 = num2;
		this.where1 = where1;
		this.where2 = where2;
		this.mode = mode;
	}
	
	public String toString(){
		String ret = new String();
		int[] kart1; int[] kart2;
		kart1 = Convert.indexToKartes(where1);
		kart2 = Convert.indexToKartes(where2);
		ret = "Numbers " + String.valueOf(num1) + "," + String.valueOf(num2) +
			  " exist hidden int (" + String.valueOf(kart1[0]) + "," +
			  String.valueOf(kart1[1]) + ") and (" + String.valueOf(kart2[0]) +
			  "," + String.valueOf(kart2[1]) + ").";
		return ret;
	}

	public int getNum1() {
		return num1;
	}

	public int getNum2() {
		return num2;
	}

	public int getWhere1() {
		return where1;
	}

	public int getWhere2() {
		return where2;
	}
	
	public int getMode(){
		return mode;
	}
}