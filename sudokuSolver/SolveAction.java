/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudokuSolver;

public class SolveAction {
	private short action;
	private int number;
	private int index;
	
	public static final short LOOK_LINE = 1;
	public static final short LOOK_COLUMN = 2;
	public static final short LOOK_BOX = 3;
	public static final short LOOK_SQUARE = 4;
	public static final short SET_IMPOSSIBLE = 5;
	
	public SolveAction(short action, int index, int number){
		this.action = action;
		this.index = index;
		this.number = number;
	}
	
	public short getAction(){return action;}
	public int getNumber(){return number;}
	public int getIndex(){return index;}
}
