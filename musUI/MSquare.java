/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.JButton;

/**
 * <p><code>MSquare</code> is the main cell of the Sudoku board, which is 
 * implemented in <code>GamePanel</code> class.</p>
 * <p>It provides the ability to set/reset background colors and 
 * track marked/fixed condition.</p>
 * @author Visvardis Marios
 * @since 0.5
 * TODO Possibly provide an in-square show possibles feature.
 */
@SuppressWarnings("serial")
public class MSquare extends JButton{
	private Color standard;
	private Color markColor;
	private boolean fixed;
	private boolean marked;
	private int markState;
	
	public MSquare(){
		super();
		setMargin(new Insets(0,0,0,0));
		setFont(new Font("Arial", Font.PLAIN, 18));
		markState = 0;
	}
	
	public void setBackgroundColor(Color col){
		setBackground(col);
	}
	
	public void setDefaultBackgroundColor(Color col){
		standard = col;
		setBackground(col);
	}
	
	public void restoreBackgroundColor(){
		if(isMarked()){
			setBackground(markColor);
		}else{
			setBackground(standard);
		}
	}
	
	public void setFixed(boolean state){fixed = state;}
	public boolean isFixed(){return fixed;}
	public boolean isMarked(){return marked;}
	
	public void mark(Color col){
		setBackground(col);
		markColor = col;
		marked = true;
		markState++;
	}
	
	public void unmark(){
		marked = false;
		markState = 0;
		restoreBackgroundColor();
	}
	
	public int getMarkState(){
		return markState;
	}
	
	public void animate(){
		Thread t = new Thread(new Runnable(){
			public void run(){
				setBackground(new Color(225, 225, 225));
				try{Thread.sleep(100);}catch (InterruptedException e){}
				setBackground(new Color(181, 202, 185));
				try{Thread.sleep(200);}catch (InterruptedException e){}
				setBackground(new Color(141, 162, 145));
				try{Thread.sleep(400);}catch (InterruptedException e){}
				setBackground(new Color(181, 202, 185));
				try{Thread.sleep(200);}catch (InterruptedException e){}
				setBackground(new Color(225, 225, 225));
				try{Thread.sleep(100);}catch (InterruptedException e){}
				
				setBackground(standard);
			}
		});
		t.start();
	}
}


