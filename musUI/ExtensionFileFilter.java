/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * MUSoSu Project
 * A file filter that checks and validates only the extension of a given
 * filename. It is used by <code>OpenDialog</code>, <code>SaveDialog</code>, etc
 * to filter filenames.
 * @author Visvardis Marios
 *
 */
public class ExtensionFileFilter extends FileFilter{
	private String extension;
	private String description;
	
	public ExtensionFileFilter(String extension, String description){
		this.extension = extension.toLowerCase();
		this.description = description;
	}
	public boolean accept(File file){
		if(file.isDirectory())
			return true;
		if(file.getName().toLowerCase().endsWith(extension))
			return true;
		return false;
	}
	
	public String getDescription(){
		return description;
	}
}