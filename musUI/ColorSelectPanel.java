/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * <p><code>ColorSelectPanel</code> is just a panel used in the preferences dialog
 * for selecting custom colors.<p>
 * @author Visvardis Marios
 * @todo switch to GridBagLayout
 */
public class ColorSelectPanel extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	private Color color;
	private JFrame parent;
	private JButton b;
	
	public ColorSelectPanel(JFrame parent, Color col, String text){
		this.color = col;
		this.parent = parent;
		setLayout(new GridLayout(1, 2, 2, 20));
		b = new JButton();
		b.setBackground(color);
		b.addActionListener(this);
		JTextArea ta = new JTextArea();
		ta.setText(text);
		ta.setEditable(false);
		ta.setFont(new Font("Arial", Font.BOLD, 12));
		ta.setBorder(BorderFactory.createEtchedBorder());
		add(ta);
		add(b);
	}
	
	public Color getColor(){
		return color;
	}
	
	public void actionPerformed(ActionEvent e){
		ColorDialog cd = new ColorDialog(parent, "Select Color", color);
		color = cd.getColor();
		b.setBackground(color);
	}
}
