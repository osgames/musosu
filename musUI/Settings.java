/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import com.sun.org.apache.xerces.internal.parsers.SAXParser;

 

/**
 * <p><code>Settings</code> class holds all the settings of the application.
 * It provides the ability to load settings from an XML file using a SAX parser
 * and the ability to save the data on an XML. Loading and saving does not interfere
 * any XML validating. 
 * @author Visvardis Marios
 */
public class Settings {
	private boolean showTrace;
	private boolean showToolTip;
	private Color highlightColor;
	private Color buttonBackColor;
	private Color fixedBackColor;
	private Color errorBackColor;
	private Color panelBackColor;
	private Color markColorA, markColorB, markColorC, markColorD;
//	private String language; TODO
	
	public Settings(){
	
	}
	
	public void loadFromFile(InputStream input){
        InputSource inputSource = new InputSource();
        inputSource.setByteStream(input);
        load(inputSource);
      }
      
      public void loadFromFile(String input){
        try{
          FileInputStream fInputStream = new FileInputStream(input);
          InputSource inputSource = new InputSource();
          InputStream inputStream = (InputStream) fInputStream;
          inputSource.setByteStream(inputStream);
          load(inputSource);
        }catch(java.io.FileNotFoundException e){
          System.out.println("File does not exist.");
        }
      }
      
	private void load(InputSource input){
		try {
			XMLReader parser = new SAXParser();    
			MContentHandler mCH = new MContentHandler();
			parser.setContentHandler(mCH);
			parser.parse(input);
			setHighlightColor(mCH.getHighlightColor());
			setButtonBackColor(mCH.getButtonBackColor());
			setErrorBackColor(mCH.getErrorBackColor());
			setFixedBackColor(mCH.getFixedBackColor());
			setPanelBackColor(mCH.getPanelBackColor());
			setMarkColorA(mCH.getMarkColorA());
			setMarkColorB(mCH.getMarkColorB());
			setMarkColorC(mCH.getMarkColorC());
			setMarkColorD(mCH.getMarkColorD());
			setShowTrace(mCH.getShowTrace());
			setShowToolTip(mCH.getShowToolTip());
		}catch(IOException e) {
		    System.out.println("Error reading URI: " + e.getMessage( ));
		} catch (SAXException e) {
		    System.out.println("Error in parsing: " + e.getMessage( ));
		}
	}
	
    public void saveToFile(String fileName){
    	String xml = createXMLString();
    	PrintStream MyOutput = null;
		try{
			MyOutput = new PrintStream(new FileOutputStream(fileName));
		}
		catch (IOException e){
			System.out.println("Cannot save file.");
		}
		MyOutput.print(xml);
		MyOutput.close();
    }
    
    private String getXMLColor(Color col){
    	String xmlStr = new String();
    	xmlStr  = "   <red>" + String.valueOf(col.getRed()) + "</red>\n";
		xmlStr += "   <green>" + String.valueOf(col.getGreen()) + "</green>\n";
		xmlStr += "   <blue>" + String.valueOf(col.getBlue()) + "</blue>\n";
		return xmlStr;
    }
    
    private String createXMLString(){
		String xmlStr = new String();
		xmlStr = "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>\n";
		xmlStr += "<!-- File created by MUSoSu: Marios Sudoku to store user settings -->\n\n";
		xmlStr += "<MUSoSu_Settings>\n";
		
		xmlStr += "<showTrace>\n";
		if(showTrace) xmlStr += "true";
		else xmlStr += "false";
		xmlStr += "</showTrace>\n";
		
		xmlStr += "<showToolTip>\n";
		if(showToolTip) xmlStr += "true";
		else xmlStr += "false";
		xmlStr += "</showToolTip>\n";
		
		xmlStr += "<highlightColor>\n";
		xmlStr += getXMLColor(highlightColor);
		xmlStr += "</highlightColor>\n";
		
		xmlStr += "<buttonBackColor>\n";
		xmlStr += getXMLColor(buttonBackColor);
		xmlStr += "</buttonBackColor>\n";
		
		xmlStr += "<panelBackColor>\n";
		xmlStr += getXMLColor(panelBackColor);
		xmlStr += "</panelBackColor>\n";
		
		xmlStr += "<errorBackColor>\n";
		xmlStr += getXMLColor(errorBackColor);
		xmlStr += "</errorBackColor>\n";
		
		xmlStr += "<fixedBackColor>";
		xmlStr += getXMLColor(fixedBackColor);
		xmlStr += "</fixedBackColor>\n";
		
		xmlStr += "<markColorA>\n";
		xmlStr += getXMLColor(markColorA);
		xmlStr += "</markColorA>\n";
		
		xmlStr += "<markColorB>\n";
		xmlStr += getXMLColor(markColorB);
		xmlStr += "</markColorB>\n";
		
		xmlStr += "<markColorC>\n";
		xmlStr += getXMLColor(markColorC);
		xmlStr += "</markColorC>\n";
		
		xmlStr += "<markColorD>\n";
		xmlStr += getXMLColor(markColorD);
		xmlStr += "</markColorD>\n";
		
		xmlStr += "</MUSoSu_Settings>";
		
		return xmlStr;
    }
	
    // GETS and SETS
	public boolean getShowTrace(){return showTrace;}
	public boolean getShowToolTip(){return showToolTip;}
    public Color getFixedBackColor(){return fixedBackColor;}
	public Color getErrorBackColor(){return errorBackColor;}
	public Color getPanelBackColor(){return panelBackColor;}
	public Color getHighlightColor(){return highlightColor;}
	public Color getButtonBackColor(){return buttonBackColor;}
	public Color getMarkColorA(){return markColorA;}
	public Color getMarkColorB(){return markColorB;}
	public Color getMarkColorC(){return markColorC;}
	public Color getMarkColorD(){return markColorD;}
	public void setShowTrace(boolean state){showTrace = state;}
	public void setShowToolTip(boolean state){showToolTip = state;}
	public void setFixedBackColor(Color col){fixedBackColor = col;}
	public void setErrorBackColor(Color col){errorBackColor = col;}
	public void setPanelBackColor(Color col){panelBackColor = col;}
	public void setButtonBackColor(Color col){buttonBackColor = col;}
	public void setHighlightColor(Color col){highlightColor = col;}
	public void setMarkColorA(Color col){markColorA = col;}
	public void setMarkColorB(Color col){markColorB = col;}
	public void setMarkColorC(Color col){markColorC = col;}
	public void setMarkColorD(Color col){markColorD = col;}
}



class MContentHandler implements ContentHandler {
	
	public void setDocumentLocator(Locator locator) {
	}
  
	public void startDocument( ) throws SAXException {
		i = 0;j= 0;r=0;g=0;b=0;st=true;stt=true;
		System.out.print("Parsing XML document started ... ");
	}
   
	public void endDocument( ) throws SAXException {
		System.out.print("OK\n");
	}
  
	public void processingInstruction(String target, String data) throws SAXException {
	}
 
	public void startPrefixMapping(String prefix, String uri) {
	
	}

	public void endPrefixMapping(String prefix) {
	
	}

	public void startElement(String namespaceURI, String localName,
                        String rawName, Attributes atts) throws SAXException {	
		
		if(rawName == "highlightColor")	i = 1;
		else if(rawName == "buttonBackColor") i = 2;
		else if(rawName == "panelBackColor") i = 3;
		else if(rawName == "errorBackColor") i = 4;
		else if(rawName == "fixedBackColor") i = 5;
		else if(rawName == "markColorA") i = 6;
		else if(rawName == "markColorB") i = 7;
		else if(rawName == "markColorC") i = 8;
		else if(rawName == "markColorD") i = 9;
		else if(rawName == "showTrace") i = 15;
		else if(rawName == "showToolTip") i = 16;
		else if(rawName == "red") j = 1;
		else if(rawName == "green")	j = 2;
		else if(rawName == "blue")	j = 3 ;
	}

	public void endElement(String namespaceURI, String localName,
                        String rawName) throws SAXException {
		if(j == 3){
			if(i == 1) highlightColor = new Color(r, g, b);
			if(i == 2) buttonBackColor = new Color(r, g, b);
			if(i == 3) panelBackColor = new Color(r, g, b);
			if(i == 4) errorBackColor = new Color(r, g, b);
			if(i == 5) fixedBackColor = new Color(r, g, b);
			if(i == 6) markColorA = new Color(r, g, b);
			if(i == 7) markColorB = new Color(r, g, b);
			if(i == 8) markColorC = new Color(r, g, b);
			if(i == 9) markColorD = new Color(r, g, b);
		}
		if(i == 15) showTrace = st;
		if(i == 16) showToolTip = stt;
		
		j = 0;
		if(rawName == "highlightColor" ||rawName == "buttonBackColor" 
			|| rawName == "panelBackColor" || rawName == "errorBackColor" 
			|| rawName == "fixedBackColor"
			|| rawName == "markColorA" || rawName == "markColorB"
			|| rawName == "markColorC" || rawName == "markColorD"
			|| rawName == "showTrace" || rawName == "showToolTip"){
			i = 0;
			j = 0;
		}
	}
 
	public void characters(char[] ch, int start, int end) throws SAXException {
		String s = new String(ch, start, end);
		if(j != 0){
			if(j == 1) r = Integer.valueOf(s);
			if(j == 2) g = Integer.valueOf(s);
			if(j == 3) b = Integer.valueOf(s);
		}
		if(i == 15){
			if(s.indexOf("true") != -1)	st = true;
			else st  = false;
		}
		if(i == 16){
			if(s.indexOf("true") != -1) stt = true;
			else stt = false;
		}
	}
    
	public void ignorableWhitespace(char[] ch, int start, int end) throws SAXException {
	}
    
	public void skippedEntity(String name) throws SAXException {
 
	}
	
	private int i, j;
	private boolean st, stt;
	private int r, g, b;
	public boolean getShowTrace(){return showTrace;}
	public boolean getShowToolTip(){return showToolTip;}
	public Color getFixedBackColor(){return fixedBackColor;}
	public Color getErrorBackColor(){return errorBackColor;}
	public Color getPanelBackColor(){return panelBackColor;}
	public Color getHighlightColor(){return highlightColor;}
	public Color getButtonBackColor(){return buttonBackColor;}
	public Color getMarkColorA(){return markColorA;}
	public Color getMarkColorB(){return markColorB;}
	public Color getMarkColorC(){return markColorC;}
	public Color getMarkColorD(){return markColorD;}
	
	private boolean showTrace;
	private boolean showToolTip;
	private Color highlightColor;
	private Color buttonBackColor;
	private Color fixedBackColor;
	private Color errorBackColor;
	private Color panelBackColor;
	private Color markColorA, markColorB, markColorC, markColorD;
}
