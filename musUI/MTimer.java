/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * <p><code>MTimer</code> is a JLabel child that is used to count time passed
 * in hours:minutes:seconds format using <code>System.currentTimeMillis()</code>
 * (there was no need for using nanoTimer).</p>
 * usage:
 * <pre>
 * MTimer myTimer = new MTimer();	
 * long start = System.currentTimeMillis();
 * myTimer.setStart(start);
 * Thread timerThread = new Thread(myTimer);
 * </pre>
 * <p>The timer can finally be stopped by calling <pre>timerThread.interrupt();</pre>
 * @author Visvardis Marios
 */
public class MTimer extends JLabel implements Runnable{
	private static final long serialVersionUID = 1L;
	private long startTime; 
	private boolean doCount;
	
	public void setDoCount(boolean state){
		doCount = state;
	}
	
	public MTimer(){
		setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
		setText("00:00");
		doCount = true;
	}
	
	public void setStart(long start){
		this.startTime = start;
	}
	
	public String getTime(){
		long timeLong = System.currentTimeMillis() - startTime;
		long timeSecs = timeLong / 1000;
		long hours = timeSecs / 3600;
		long mins = (timeSecs % 3600) / 60;
		long secs = (timeSecs % 3600) % 60;
		String timeStr = new String("");
		if(hours != 0) 
			timeStr += String.valueOf(hours) + ":";
		String minsStr = String.valueOf(mins);
		if(minsStr.length() == 1) 
			minsStr = "0" + minsStr;
		timeStr += minsStr + ":";
		String secsStr = String.valueOf(secs);
		if(secsStr.length() == 1) 
			secsStr = "0" + secsStr;
		timeStr += secsStr;
		
		return timeStr;
	}
	
	public void run(){
		while(doCount){
			try{
				Thread.sleep(1000);
			}catch (InterruptedException e){
				
			}
			setText(getTime());
		}
	}
}
