/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * <p><code>AboutDialog</code> is a rather simple about dialog box for the MUSoSu
 * application. Just instansiate and setVisible(true).</p>
 * @author Visvardis Marios
 * @version 0.3
 */
public class AboutDialog extends JDialog implements ActionListener{
	private static final long serialVersionUID = 1L;

	public AboutDialog(JFrame parent, String title, String version){
		super(parent, title, true);
		
		if(parent != null){
			  Dimension parentSize = parent.getSize(); // Parent size
			  Point p = parent.getLocation();          // Parent position
			  setLocation(p.x+parentSize.width/4,p.y+parentSize.height/4);
		}
		
//		getContentPane().setLayout(new GridLayout(3,1,1,1));
		getContentPane().setLayout(new BorderLayout());

		
		
		ImagePanel imagePanel;
		try{
                        ClassLoader loader = this.getClass().getClassLoader();
                        InputStream imStream = loader.getResourceAsStream("musosu_logo.gif");
                        Image img = ImageIO.read(imStream);
			imagePanel = new ImagePanel(img);
//			getContentPane().add(imagePanel);
			imagePanel.setPreferredSize(new Dimension(200, 50));
			getContentPane().add(imagePanel, BorderLayout.NORTH);
		}catch (Exception e){
			System.out.println("Failed to open image");
		}
		
		String txt = new String
		        (  " October 2006 - April 2008 \n"
		         + " Version: " + version + "\n" 
			     + " Author: Visvardis Marios \n"
			     + " Licence: GPL version 3\n"
			     + " e-mail: visvardis.marios@gmail.com \n");
		JTextArea text = new JTextArea(txt);
		text.setFont(new Font("Arial", Font.BOLD, 10));
		text.setEditable(false);
		
		JPanel textPanel = new JPanel();
		textPanel.add(text);
		textPanel.setBackground(Color.WHITE);
		textPanel.setBorder(BorderFactory.createEtchedBorder());
		
//		getContentPane().add(textPanel);
		getContentPane().add(textPanel, BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBackground(Color.WHITE);
		JButton button = new JButton();
		button.setBackground(Color.WHITE);
		button.setText("OK");
		button.addActionListener(this);
		buttonPanel.add(button);
//		getContentPane().add(buttonPanel);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		
//		setPreferredSize(new Dimension(200, 170));
		setResizable(false);
		pack();
	}
	
	public void actionPerformed(ActionEvent evt){
		setVisible(false);
		dispose();
	}
}
