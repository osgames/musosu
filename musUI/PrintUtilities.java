/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.*;
import javax.swing.*;
import java.awt.print.*;

/**
 * <code>PrintUtilities</code>
 * <p>Class for printing Swing Components. </p>
 * <p>You can call the static method <code>printComponent</code> to
 * instantly print any comonent, or instantiate the class for more
 * cusomizations.</p> 
 * <p>For the purposes of MUSoSu i use only the feature 
 * for one page printing and i have set the width of the printed panel
 * to be 1/3 of the printable area of the paper.</p>
 * 
 * 
 * Based on article found at <a href="http://www.developerdotstar.com/community/node/124/print">
 * http://www.developerdotstar.com/community/node/124/print</a>
 * TODO print many sudokus in one page
 * @author Visvardis Marios
 */

public class PrintUtilities implements Printable {
	private Component componentToBePrinted;
	
	public static void printComponent(Component c) {
		new PrintUtilities(c).print();
	}
	
	public PrintUtilities(Component componentToBePrinted) {
		this.componentToBePrinted = componentToBePrinted;
	}
  
	public void print(){
		PrinterJob printJob = PrinterJob.getPrinterJob();
		printJob.setPrintable(this);
		if (printJob.printDialog())
			try{
				System.out.println("Calling PrintJob.print()");
				printJob.print();
				System.out.println("End PrintJob.print()");
			}
			catch (PrinterException pe){
				System.out.println("Error printing: " + pe);
			}
	}
  
	public int print(Graphics g, PageFormat pf, int pageIndex) {
		int response = NO_SUCH_PAGE;
		Graphics2D g2 = (Graphics2D) g;
		// for faster printing, turn off double buffering
		disableDoubleBuffering(componentToBePrinted);
		Dimension d = componentToBePrinted.getSize(); //get size of document
		double panelWidth = d.width; //width in pixels
		double panelHeight = d.height; //height in pixels
		double pageHeight = pf.getImageableHeight(); //height of printer page
		double pageWidth = pf.getImageableWidth(); //width of printer page
		
// For normal use prefer the commented line
//		double scale = pageWidth / panelWidth;
		double scale = (pageWidth / panelWidth) / 2;
		int totalNumPages = (int) Math.ceil(scale * panelHeight / pageHeight);
		// make sure not print empty pages
		if (pageIndex >= totalNumPages) {
			response = NO_SUCH_PAGE;
		}else{
			// shift Graphic to line up with beginning of print-imageable region
			g2.translate(pf.getImageableX(), pf.getImageableY());
			// shift Graphic to line up with beginning of next page to print
			g2.translate(0f, -pageIndex * pageHeight);
			// scale the page so the width fits...
			g2.scale(scale, scale);
			componentToBePrinted.paint(g2); //repaint the page for printing
			enableDoubleBuffering(componentToBePrinted);
			response = Printable.PAGE_EXISTS;
		}
		return response;
	}
  
	public static void disableDoubleBuffering(Component c) {
		RepaintManager currentManager = RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(false);
	}
  
	public static void enableDoubleBuffering(Component c) {
		RepaintManager currentManager = RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(true);
	}
}