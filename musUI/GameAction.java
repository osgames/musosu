/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

public class GameAction {
	public static final int ADDITION = 1;
	public static final int REMOVAL = 2;
	
	private int action;
	private int index;
	private int number;
	
	public GameAction(int action, int index, int number){
		this.action = action;
		this.index = index;
		this.number = number;
	}
	
	public int getAction(){return action;}
	public int getIndex(){return index;}
	public int getNumber(){return number;}
}
