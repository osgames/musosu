/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class GetStrDialog extends JDialog implements ActionListener{
	public GetStrDialog(JFrame parent, String str){
		super(parent, "Sudoku string dump", true);
		
		if(parent != null){
			  Dimension parentSize = parent.getSize(); // Parent size
			  Point p = parent.getLocation();          // Parent position
			  setLocation(p.x,p.y+parentSize.height/3);
		}
		
		JPanel center = new JPanel();
		JPanel bottom = new JPanel();
		JTextArea ta = new JTextArea();
		ta.setText(str);
		ta.setFont(new Font("Monospace", Font.PLAIN, 9));
		ta.setEditable(false);
		JButton ok = new JButton("OK");
		ok.setBackground(Color.WHITE);
		ok.addActionListener(this);
		center.add(ta);
		bottom.add(ok);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(center, BorderLayout.CENTER);
		getContentPane().add(bottom, BorderLayout.SOUTH);
		pack();
		this.setResizable(false);
	}
	
	public void actionPerformed(ActionEvent evt){
		setVisible(false);
		dispose();
	}
}
