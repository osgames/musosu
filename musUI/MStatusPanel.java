/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import sudoku.Sudoku;

/**
 * MUSoSu Project
 * <p><code>MStatusPanel</code> implements a status panel for the main frame
 * of MUSoSu application. It is informed about changes in the Sudoku game/object
 * by events fired by <code>Sudoku</code> and delivered by <code>Mui</code> 
 * class.</p>
 * <p>The status bar contains a general purpose JLabel for printing comments on 
 * user actions, a progress bar that tracks game progrees in terms of 
 * completed/total squares and a JLabel showing time lapsed implemented 
 * in <code>MTimer</code> class.  
 * @author Visvardis Marios
 * @see MTimer
 * @version 0.1
 */
@SuppressWarnings("serial")
public class MStatusPanel extends JPanel{
	private JLabel statusLabel;
	private JLabel difficultyLabel;
	private JProgressBar pb;
	private MTimer time;
	private long startTime;
	private Thread timeThread;
	private JPanel eastPanel;	
	
	public MStatusPanel(){
		super();
		statusLabel = new JLabel("MUSoSu - Marios Sudoku");
		statusLabel.setVisible(true);
		
		time = new MTimer();
		time.setVisible(false);
		
		difficultyLabel = new JLabel("");
		difficultyLabel.setToolTipText("Estimated difficulty");
		difficultyLabel.setVisible(false);
		pb = new JProgressBar();
		pb.setBackground(Color.LIGHT_GRAY);
		pb.setForeground(new Color(194, 190, 149));
		pb.setValue(1);
		pb.setPreferredSize(new Dimension(70, 10));
		pb.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY,1));
		pb.setVisible(false);
		eastPanel = new JPanel();
		eastPanel.setBackground(Color.LIGHT_GRAY);
		eastPanel.setLayout(new BorderLayout(2, 2));
		eastPanel.add(difficultyLabel, BorderLayout.WEST);
		eastPanel.add(pb, BorderLayout.EAST);
		
		setLayout(new BorderLayout(10, 10));
		setBackground(Color.LIGHT_GRAY);
		
		add(time, BorderLayout.WEST);
		add(statusLabel, BorderLayout.CENTER);
		add(eastPanel, BorderLayout.EAST);
	}
	
	public void gameUpdatedEvent(Sudoku game){
		int completed;
		
		if(game != null) completed = game.getCompleted();
		else completed = 0;
		int p = (100 * completed) / 81;
		pb.setValue(p);
		pb.setToolTipText(new String(String.valueOf(p) + "% Completed. " 
				  + String.valueOf(81 - completed) + " squares remaining."));
		showProgress();
		showTime();
		showDifficulty();
		eastPanel.setVisible(true);
	}
	
	public void startTimer(){
		startTime = System.currentTimeMillis();
		time.setStart(startTime);
		timeThread = new Thread(time);	
		timeThread.start();
	}
	
	public void stopTimer(){
		time.setDoCount(false);
	}
	
	public String getTimeString(){return time.getText();}
	
	public void showTime(){time.setVisible(true);}
	public void showProgress(){pb.setVisible(true);}
	public void showDifficulty(){difficultyLabel.setVisible(true);}
	
	public void setStatusLabel(String str){
		statusLabel.setText(str);
		statusLabel.setToolTipText(str);
	}
	
	public void setStopped(){
		eastPanel.setVisible(false);
		time.setVisible(false);
	}
	
	public void setDifficulty(double estimatedDifficulty){
		String estDiff = String.valueOf(estimatedDifficulty);
		if(estDiff.length() > 4) estDiff = estDiff.substring(0, 3);
		difficultyLabel.setText(estDiff);
		showDifficulty();
	}
}
