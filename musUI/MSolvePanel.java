/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
//import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import sudoku.Sudoku;
import sudokuSolver.SolveMethod;
import sudokuSolver.SolvedSquare;
import sudokuSolver.Solver;

@SuppressWarnings("serial")
public class MSolvePanel extends JPanel implements ActionListener{
	private JTextArea textArea;
	private JButton solveButton;;
	private JPanel top;
	private JPanel methods;
	private Vector<JLabel> success;
	private Vector<JPanel> selectionPanels;
	
	private Sudoku game;
	private GamePanel gp;
	private Solver solver;
	private int[] selection;
	private int numberOfMethods;
	
	public MSolvePanel(GamePanel gp){
		super();
		this.gp = gp;
//		JScrollBar sb = new JScrollBar();
//		sb.v
	}
	
	private void initSolver(){
		solver = new Solver();
		numberOfMethods = solver.getNumberOfMethods();
		selection = new int[numberOfMethods];
		for(int i = 0; i < numberOfMethods; i++)
			selection[i] = i;
	}
	
	public void setSudoku(Sudoku game){
		initSolver();
		this.game = game;
		solver.setGame(this.game);
		initComponents();
	}
	
	public void actionPerformed(ActionEvent evt){
		AbstractButton ob = (AbstractButton) evt.getSource();
		String obName = ob.getName();
		
		if(obName == "solve"){solve();}
		else if(obName.contains("ch")){
			JCheckBox cb = (JCheckBox) ob;
			String num = obName.substring(2);

			int index = Integer.valueOf(num);
			if(cb.isSelected()){
				int[] newSelection = new int[selection.length + 1];
				int tmp;
				for(tmp = 0; tmp < selection.length; tmp++){
					newSelection[tmp] = selection[tmp];
				}
				newSelection[tmp]  = index;
				selection = newSelection;
			}else{
				int[] newSelection = new int[selection.length - 1];
				int tt = 0;
				for(int tmp = 0; tmp < selection.length - 1; tmp++){
					if(selection[tmp] != index){
						newSelection[tt] = selection[tmp];
						tt++;
					}
				}
				selection = newSelection;
			}
		}else if(obName.contains("b")){
			String num = obName.substring(1);
			int index = Integer.valueOf(num);
			solver.setGame(game);
			Vector <SolvedSquare> sqv = solver.runMethodStandalone(index);
			if(sqv != null)
				addToGame(sqv);
			refreshSuccess();
		}	
	}
	
	private void addToGame(Vector<SolvedSquare> sqv){
		for(SolvedSquare ssq: sqv)
			gp.addToGame(ssq.getValue(), ssq.getIndex());
	}
	
	private void initComponents(){
		textArea = new JTextArea();
		textArea.setText("Click solve to automatically\nfind the solution using" +
				" the\nchecked methods, or \nmanualy click the desired\nmethod");
		textArea.setBackground(new Color(224, 220, 179));
		textArea.setMargin(new Insets(5, 5, 5, 5));
		textArea.setFont(new Font("Arial", Font.PLAIN, 14));
		textArea.setSize(this.getWidth(), 50);
		
		solveButton  = new JButton("Solve");
		solveButton.setName("solve");
		solveButton.addActionListener(this); 
		solveButton.setBackground(Color.WHITE);
		
		top = new JPanel();
		top.setLayout(new BorderLayout());
		top.add(textArea, BorderLayout.NORTH);
		top.add(solveButton, BorderLayout.CENTER);
		
		selectionPanels = new Vector<JPanel>();
		methods = new JPanel();
		success = new Vector<JLabel>();
		
		int category = -1;
		for(int s = 0; s < solver.getNumberOfMethods(); s++){
			JPanel p = new JPanel();
			p.setPreferredSize(new Dimension(190, 15));
			p.setLayout(new BorderLayout());
			
			JButton b = new JButton(solver.getMethodName(s));
			b.setName("b" + String.valueOf(s));
			b.setFont(new Font("Arial", Font.PLAIN, 9));
			b.setBackground(Color.WHITE);
			b.setMargin(new Insets(1,1,1,1));
			b.addActionListener(this);
				
			JCheckBox cb = new JCheckBox("");
			cb.setName("ch" + String.valueOf(s));
			cb.setSelected(true);
			cb.addActionListener(this);
			
			JLabel l = new JLabel(" ");
			l.setFont(new Font("Arial", Font.PLAIN, 9));
			l.setBorder(new EmptyBorder(new Insets(0, 5, 0, 5)));
			success.add(s, l);
			
			p.add(cb, BorderLayout.WEST); 
			p.add(b, BorderLayout.CENTER);
			p.add(l, BorderLayout.EAST);
			
			if(solver.getMethodType(s) != category){
				category = solver.getMethodType(s);
				methods.add(new CategoryLabel(category));
			}
			methods.add(p);
			selectionPanels.add(s, p);
		}
		
		this.setLayout(new BorderLayout());
		this.add(top, BorderLayout.NORTH);
		this.add(methods, BorderLayout.CENTER);
		this.setPreferredSize(new Dimension(200, gp.getHeight()));
		this.setVisible(true);
	}
	
	public void solve(){
		solver.setGame(game);
		solver.solveWithSelectedMethods(selection);
		addToGame(solver.getSolvedSquares());
		this.refreshSuccess();
	}
	
	private void refreshSuccess(){
		for(int i = 0; i < solver.getNumberOfMethods(); i++){
			String contrib = String.valueOf(solver.getMethodContribution(i));
			success.get(i).setText(contrib);
			String time = String.valueOf(solver.getMethodUptimeMillis(i));
			time += "ms total execution time";
			success.get(i).setToolTipText(time);
			if(solver.getMethodContribution(i) == 0){
				success.get(i).setText("  ");
			}
		}
	}
}

@SuppressWarnings("serial") 
class CategoryLabel extends JLabel{
	public CategoryLabel(int category){
		super();
		String txt = new String();
		
		switch(category){
		case SolveMethod.ELIMINATION:
			txt = "Simple elimination Methods";
			break;
		case SolveMethod.MODERATE:
			txt = "Moderate Methods";
			break;
		case SolveMethod.ADVANCED:
			txt = "Advanced Methods";
			break;
		case SolveMethod.EVIL:
			txt = "Evil Methods";
			break;
		case SolveMethod.BRUTE_FORCE:
			txt = "Brute Force Methods";
			break;
		}
		
		setFont(new Font("Arial", Font.BOLD, 11));
		setText(txt);
		setSize(180, 13);
		setVisible(true);
	}
}
