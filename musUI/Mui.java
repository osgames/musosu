/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.InputStream;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import sudoku.Sudoku;
import sudoku.SudokuEvent;
import sudoku.SudokuListener;
import sudokuSolver.SolvedSquare;
import sudokuSolver.Solver;
/**
 * <p>class <code>Mui</code> does most of the GUI work and implements the
 * main frame of the application.<br>
 * Creates the menu and hanldes it's events (implementing 
 * <code>ActionListener</code> interface).<br>Initialises all the components</p>
 * 
 * @see GamePanel
 * @see MStatusPanel
 * @author Visvardis Marios
 * 
 */
@SuppressWarnings("serial")
public class Mui extends JFrame implements ActionListener{
	// Visual Components
	private JMenuBar menuBar = new JMenuBar();
	private JLabel label;
	private JPanel superPanel;
	private GamePanel panel;
	private MStatusPanel statusPanel;
	private JPanel newCustomPanel;
	private JPanel southPanel;
	private JPanel titlePanel;
	private JPanel middle;
	private MSolvePanel msp;
	
	private JButton save;
	private JButton savenopen;
	private JButton cancel;
	private JButton getStr;
	
	// game represents the game currently processed and displayed
	private Sudoku game;
	private Sudoku virgin;  // A untouched copy of the original game
	
	// Solver
	private Solver solver;
	
	// Holding the path of the last saved file or null if not defined
	private String saveName;
	
	// preloaded
	private Vector<String> preloaded;
	private Vector<String> preloadedNames;
	
	private Settings settings;
	
	private String version;
	
	// For the Menu
	private JMenu fileMenu; 
    private JMenu viewMenu;
    private JMenu solverMenu;
    private JMenu aboutMenu;
    private JMenu newMenu;
    
    private JMenuItem newCustomM;
    private JMenuItem newM;
    private JMenuItem openM;
    private JMenu openPresetM;
    JMenuItem[] preMenuItems;
    private JMenuItem saveAsM;
    private JMenuItem restartM;
    private JMenuItem closeM;
    private JMenuItem printM;
    private JMenuItem exitM;
    private JCheckBoxMenuItem showSolvePanelM;
    private JCheckBoxMenuItem showTraceM;
    private JCheckBoxMenuItem showTTM;
    private JMenuItem clearMarksM;
    private JMenuItem saveThemeM; 
    private JMenuItem loadThemeM;
    private JMenuItem loadDefaultThemeM;
    private JMenuItem saveDefaultM;
    private JMenuItem prefsM;
    private JMenuItem undoM;
    private JMenuItem redoM;
    private JMenuItem hintM;
    private JMenuItem solveAtOnceM;
    private JMenuItem solveM;
    private JMenuItem aboutM;
    
	/**
	 * Mui - Constructor
	 * All the work is done in initComponents, createMenu, initSettings
	 * is loaded or created.
	 *
	 */
	public Mui(){
		setIcon();
		initSettings();
		createMenu();
		initComponents();
		loadSettings(settings);
		setVisible(false);
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	
	private void initSettings(){
	    settings = new Settings();
	    String home = System.getProperty("user.home");
	    System.out.println(home);
	      String fileDIR = new String(home + File.separator +"MUSoSu");
	    File f = new File(fileDIR);
	    if(f.exists()){
	      String fileURI = new String(fileDIR + File.separator + "settings.xml");
	      if(new File(fileURI).exists()){
	        settings.loadFromFile(fileURI);
	      }
	    }else{ 
	      ClassLoader cl = this.getClass().getClassLoader();
	      InputStream in = cl.getResourceAsStream("default.xml");
	      settings.loadFromFile(in);
	    }
	  }
	
	private void loadSettings(Settings s){
		panel.setFixedBackColor(s.getFixedBackColor());
		panel.setHighlightColor(s.getHighlightColor());
		panel.setSquareBackColor(s.getButtonBackColor());
		panel.setErrorBackColor(s.getErrorBackColor());
		panel.setPanelBackColor(s.getPanelBackColor());
		panel.setShowTrace(s.getShowTrace());
		panel.setShowToolTip(s.getShowToolTip());
		panel.setMarkColorA(s.getMarkColorA());
		panel.setMarkColorB(s.getMarkColorB());
		panel.setMarkColorC(s.getMarkColorC());
		panel.setMarkColorD(s.getMarkColorD());
		showTraceM.setState(s.getShowTrace());
		showTTM.setState(s.getShowToolTip());
	}
	
	private void setIcon(){
		try{
			ClassLoader loader = this.getClass().getClassLoader();
			InputStream imStream = loader.getResourceAsStream("musosu.png");
			Image img = ImageIO.read(imStream);
			setIconImage(img);
		}catch(Exception e){
			System.out.println("Could not load icon");
		}
	}
	
	private void registerGameListener(Sudoku s){
		s.addSudokuListener(new SudokuListener(){
			public void sudokuChanged(SudokuEvent e){
				sudokuChangedEvent(e);
			}
		});
	}
	
	private void initSudoku(){
		virgin = new Sudoku(game);
		solver = new Solver();
		registerGameListener(game);
		game.fireSudokuEvent(SudokuEvent.SUDOKU_CHANGED);
		statusPanel.startTimer();
		panel.loadBoard(game);
		solver.setGame(game);
		game.setDifficulty(solver.estimateDifficulty());
		statusPanel.setDifficulty(game.getDifficulty());
		
		newCustomPanel.setVisible(false);
		restartM.setEnabled(true);
		closeM.setEnabled(true);
		saveAsM.setEnabled(true);
		printM.setEnabled(true);
		showSolvePanelM.setEnabled(true);
		hintM.setEnabled(true);
		solveM.setEnabled(true);
		solveAtOnceM.setEnabled(true);
		if(msp.isVisible()){
			msp.setSudoku(game);
		}
		panel.panelResized();
	}
	
	
	// EVENT HANDLING METHODS
	
	public void actionPerformed(ActionEvent evt){
		if(evt.getSource() == newM) newMenuEvent(evt);
		else if(evt.getSource() == newCustomM) newCustomMenuEvent(evt);
		else if(evt.getSource() == openM) openMenuEvent(evt);
		else if(evt.getSource() == restartM) restartMenuEvent(evt);
		else if(evt.getSource() == closeM) closeMenuEvent(evt);
		else if(evt.getSource() == saveAsM) saveAsMenuEvent(evt);
		else if(evt.getSource() == printM) printMenuEvent(evt);
		else if(evt.getSource() == exitM) exitMenuEvent(evt);
		else if(evt.getSource() == clearMarksM) panel.unmarkAll();
		else if(evt.getSource() == loadThemeM) loadThemeMenuEvent(evt);
		else if(evt.getSource() == loadDefaultThemeM) loadDefaultThemeMenuEvent(evt);
		else if(evt.getSource() == saveThemeM) saveThemeMenuEvent(evt);
		else if(evt.getSource() == saveDefaultM) saveDefaultMenuEvent(evt);
		else if(evt.getSource() == prefsM) prefsMenuEvent(evt);
		else if(evt.getSource() == undoM) panel.undo();
		else if(evt.getSource() == redoM) panel.redo();
		else if(evt.getSource() == hintM) hintMenuEvent(evt);
		else if(evt.getSource() == solveAtOnceM) solveAtOnceMenuEvent(evt);
		else if(evt.getSource() == solveM) solveMenuEvent(evt);
 		else if(evt.getSource() == aboutM) aboutMenuEvent(evt);
 		// Events from newCustomPanel Buttons
 		else if(evt.getSource() == getStr) getStrEvent();
 		else if(evt.getSource() == cancel) closeMenuEvent(evt);
 		else if(evt.getSource() == save) saveAsMenuEvent(evt);
 		else if(evt.getSource() == savenopen){
 			saveAsMenuEvent(evt);
 			if(this.saveName != null)
 				openFromFile(saveName);
 		}
 		else System.out.println("Action not defined...");
	}
	
	private void sudokuChangedEvent(SudokuEvent e){
		Sudoku s = (Sudoku) e.getSource();
		statusPanel.gameUpdatedEvent(s);
	}
	
	private void newMenuEvent(ActionEvent e){
		GenerateDialog generateDialog;
		generateDialog = new GenerateDialog(this, "New Sudoku");
		generateDialog.setVisible(true);

		game = generateDialog.getGame();
		if(game != null){
			setStatus("New Game Generated!");
			this.initSudoku();
		}
		
	}
	
	private void newCustomMenuEvent(ActionEvent e){
		newCustomPanel.setVisible(true);
		if(msp.isVisible()) 
			hideSolverPanel();
		closeM.setEnabled(true);
		saveAsM.setEnabled(true);
		printM.setEnabled(true);
		this.showSolvePanelM.setEnabled(false);
		this.solveM.setEnabled(false);
		
		this.game = new Sudoku();
		panel.loadBoard(game);
		panel.setVisible(true);
		
		statusPanel.setStopped();
		setStatus("Creating custom game.");
		
		this.validateTree();
	}
	
	/**
	 * Handles the event tha occurs when the "Open Game..." 
	 * MenuItem is pressed.
	 * Creates an OpenDialog, stores the desired filename,
	 * Creates an object of class Sudoku for this file and
	 * finaly calls loadBoard to display the board.
	 */
	private void openMenuEvent(ActionEvent evt){
		String fname;
		setStatus("Open Menu selected");
		final JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new ExtensionFileFilter("sud", "Sudoku file"));
		int returnVal = fc.showOpenDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION){
			fname = new String(fc.getSelectedFile().getAbsolutePath());
			openFromFile(fname);
		}
	}
	
	private void openFromFile(String fname){
		setStatus("File choosed: " + fname);
		game = new Sudoku(fname);
		initSudoku();
	}
	
	private void preloadMenuEvent(ActionEvent evt){
		JMenuItem ob = (JMenuItem) evt.getSource();
		setStatus("Loaded preloaded game");
		game = new Sudoku(ob.getName(), 4734);
		
		initSudoku();
	}
	
	private void saveAsMenuEvent(ActionEvent evt){
		String ext = "sud";
		final JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new ExtensionFileFilter(ext, "Sudoku file"));
		int returnVal = fc.showSaveDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION){
			saveName = new String(fc.getSelectedFile().getAbsolutePath());
			if(!saveName.endsWith(ext))
				saveName += ".sud";
			
			if(fc.getSelectedFile().exists()){
				if(!(JOptionPane.NO_OPTION 
				   == JOptionPane.showConfirmDialog(this, saveName +" exists. Overwrite?",
				      "Confirm Save As", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE))){
					game.saveToFile(saveName);
					setStatus("File saved: " + saveName);
				}
			}else{
				game.saveToFile(saveName);
				setStatus("File saved: " + saveName);				
			}
		}
		
	}
	
	private void restartMenuEvent(ActionEvent evt){
		game = new Sudoku(virgin);
		initSudoku();
	}
	
	private void closeMenuEvent(ActionEvent evt){
		panel.setVisible(false);
		setStatus("Does the close function have sense?");
		statusPanel.stopTimer();
		if(msp.isVisible())
			this.hideSolverPanel();
		newCustomPanel.setVisible(false);
		restartM.setEnabled(false);
		saveAsM.setEnabled(false);
		printM.setEnabled(false);
		closeM.setEnabled(false);
		showSolvePanelM.setEnabled(false);
		showSolvePanelM.setState(false);
		solveM.setEnabled(false);
		solveAtOnceM.setEnabled(false);
		hintM.setEnabled(false);
		statusPanel.setStopped();
	}
	
	private void printMenuEvent(ActionEvent evt){
		PrintUtilities.printComponent(panel);
		// TODO
		// 1) add functionality to print more than one Sudokus
		//    with Print Preview
		// 2) print a label
		// 3) to do the above, panel must be replaced
		//    with anoter printable
	}
	
	private void exitMenuEvent(ActionEvent evt){
	     processEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}
		
	private void showTraceMenuEvent(ChangeEvent evt){
		JCheckBoxMenuItem tmp = (JCheckBoxMenuItem) evt.getSource();
		boolean b = tmp.getState();
		panel.setShowTrace(b);
		settings.setShowTrace(b);
	}
	
	private void showTTMenuEvent(ChangeEvent evt){
		JCheckBoxMenuItem tmp = (JCheckBoxMenuItem) evt.getSource();
		boolean b = tmp.getState();
		panel.setShowToolTip(b);
		settings.setShowToolTip(b);
	}
	
	private void saveThemeMenuEvent(ActionEvent evt){
		String saveName;
		String ext = "xml";
		final JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new ExtensionFileFilter(ext, "XML Theme File"));
		int returnVal = fc.showSaveDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION){
			saveName = new String(fc.getSelectedFile().getAbsolutePath());
			if(!saveName.endsWith(ext))
				saveName += ".xml";
			
			if(fc.getSelectedFile().exists()){
				if(!(JOptionPane.NO_OPTION 
				   == JOptionPane.showConfirmDialog(this, saveName +" exists. Overwrite?",
				      "Confirm Save As", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE))){
					settings.saveToFile(saveName);
					setStatus("Theme File saved: " + saveName);
				}
			}else{
				settings.saveToFile(saveName);
				setStatus("Theme File saved: " + saveName);				
			}
		}
		
	}
	
	private void saveDefaultMenuEvent(ActionEvent evt){
		String dir;
		dir = System.getProperty("user.home");
		if(new File(dir + File.separator +"MUSoSu").exists()){
			String fileURI = new String(dir + File.separator +
					                    "MUSoSu" + File.separator +"settings.xml");
			settings.saveToFile(fileURI);
			setStatus("Default Settings were successfully set.");
		}else{
			setStatus("Unable to change default settings.");
		}
	}
	
	private void loadThemeMenuEvent(ActionEvent evt){
		String fname;
		final JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new ExtensionFileFilter("xml", "XML Theme Document"));
		int returnVal = fc.showOpenDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION){
			fname = new String(fc.getSelectedFile().getAbsolutePath());
			settings.loadFromFile(fname);
			loadSettings(settings);
		}
	}
	
	private void loadDefaultThemeMenuEvent(ActionEvent evt){
		settings.loadFromFile("default.xml");
		loadSettings(settings);
	}
	
	private void prefsMenuEvent(ActionEvent evt){
		preferencesDialog pd = new preferencesDialog(this, settings);
		pd.setVisible(true);
		settings = pd.getSettings();
		loadSettings(settings);
	}
	
	private void hintMenuEvent(ActionEvent evt){
		solver.setGame(game);
		SolvedSquare sq;
		if((sq = solver.getHint()) != null){
			panel.addToGame(sq.getValue(), sq.getIndex());
		}else{
			this.setStatus("Unable to provide hint");
		}
	}
	
	private void solveAtOnceMenuEvent(ActionEvent evt){
		if(game.checkFull()){
			setStatus("Game already solved.");
			return;
		}
		solver.setGame(game);
		solver.solve();
		
		Vector<SolvedSquare> ssv= solver.getSolvedSquares();
		for(SolvedSquare sq: ssv){
			panel.addToGame(sq.getValue(), sq.getIndex());
		}
		
		if(solver.isCompleted()){
			String st = String.valueOf(solver.getTotalTimeMillis());
			setStatus("Solution found in " + st + " ms");
		}else{
			setStatus("Failed to solve this sudoku.");
		}
	}
	
	private void solveMenuEvent(ActionEvent evt){
		if(!msp.isVisible())
			showSolvePanelM.doClick();
	}
	
	private void aboutMenuEvent(ActionEvent evt){
		AboutDialog aboutDialog;
		aboutDialog = new AboutDialog(this, "About MUSoSu", version);
		aboutDialog.setVisible(true);
	}
	
	// END OF EVENT HANDLING METHODS
	
	/**
	 * Sets the value of the message in the status bar. 
	 * Possibly not needed
	 */
	private void setStatus(String message){
		statusPanel.setStatusLabel(message);
	}
	
	private void createMenu(){
		setJMenuBar(menuBar);        
        fileMenu = new JMenu("File"); 
        viewMenu = new JMenu("View");
        solverMenu = new JMenu("Game");
        aboutMenu = new JMenu("Help");
        newMenu = new JMenu("New");
        
        menuBar.add(fileMenu);
        menuBar.add(viewMenu);
        menuBar.add(solverMenu);
        menuBar.add(aboutMenu);
        
        newCustomM = new JMenuItem();
        newCustomM.setText("Custom Game");
        newCustomM.addActionListener(this);
        
        newM = new JMenuItem();
        newM.setText("New Game...");
        newM.addActionListener(this);
        newM.setEnabled(true);
        
        newMenu.add(newM);
        newMenu.add(newCustomM);
        
        openM = new JMenuItem();
        openM.setText("Open Game...");
        openM.addActionListener(this);
        
        openPresetM = new JMenu();
        openPresetM.setText("Open Preset Game");
        preloadGames();
        preMenuItems = new JMenuItem[preloaded.size()];
        String tmpS, tmpN;   
        for(int i = 0; i < preloaded.size(); i++){
        	preMenuItems[i] = new JMenuItem();
        	tmpS = preloaded.get(i);
        	tmpN = preloadedNames.get(i);
        	
        	preMenuItems[i].setText(tmpN);
        	preMenuItems[i].setName(tmpS);
        	preMenuItems[i].addActionListener(new ActionListener(){
        		public void actionPerformed(ActionEvent evt){
        			preloadMenuEvent(evt);
        		}
        	});
        	openPresetM.add(preMenuItems[i]);
        }
        
        saveAsM =new JMenuItem();
        saveAsM.setText("Save Game As...");
        saveAsM.setEnabled(false);
        saveAsM.addActionListener(this);
        
        restartM = new JMenuItem();
        restartM.setText("Restart Game");
        restartM.setEnabled(false);
        restartM.addActionListener(this);
        
        closeM = new JMenuItem();
        closeM.setText("Close Game");
        closeM.setEnabled(false);
        closeM.addActionListener(this);
        
        printM = new JMenuItem();
        printM.setText("Print Board...");
        printM.setEnabled(false);
        printM.addActionListener(this);
        
        exitM = new JMenuItem();
        exitM.setText("Exit");
        exitM.addActionListener(this);
        
        showSolvePanelM = new JCheckBoxMenuItem();
        showSolvePanelM.setText("Show Solve Panel");
        showSolvePanelM.setEnabled(false);
        showSolvePanelM.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		JCheckBoxMenuItem tmp = (JCheckBoxMenuItem) evt.getSource();
        		boolean b = tmp.getState();	
        		if(b) showSolverPanel();
        		else hideSolverPanel();
        	}
        });
        
        showTraceM = new JCheckBoxMenuItem();
        showTraceM.setText("Show Trace");
        showTraceM.addChangeListener(new ChangeListener(){
        	public void stateChanged(ChangeEvent evt){
        		showTraceMenuEvent(evt);
        	}
        });
        
        showTTM = new JCheckBoxMenuItem();
        showTTM.setText("Show Possible Values");
        showTTM.addChangeListener(new ChangeListener(){
        	public void stateChanged(ChangeEvent evt){
        		showTTMenuEvent(evt);
        	}
        });
        
        clearMarksM = new JMenuItem();
        clearMarksM.setText("Clear All Marks");
        clearMarksM.addActionListener(this);
        
        saveThemeM = new JMenuItem();
        saveThemeM.setText("Save Current Settings...");
        saveThemeM.addActionListener(this);
        
        saveDefaultM = new JMenuItem();
        saveDefaultM.setText("Save Current Settings as Default");
        saveDefaultM.addActionListener(this);
        
        loadThemeM = new JMenuItem();
        loadThemeM.setText("Load Settings...");
        loadThemeM.addActionListener(this);
        
        loadDefaultThemeM = new JMenuItem();
        loadDefaultThemeM.setText("Load Default Settings");
        loadDefaultThemeM.addActionListener(this);
        
        prefsM = new JMenuItem();
        prefsM.setText("Preferences...");
        prefsM.addActionListener(this);
        
        undoM = new JMenuItem();
        undoM.setText("Undo (Ctrl-Z)");
        undoM.addActionListener(this);
        
        redoM = new JMenuItem();
        redoM.setText("Redo (Ctrl-R)");
        redoM.addActionListener(this);
        
        solveAtOnceM = new JMenuItem();
        solveAtOnceM.setText("Solve At Once");
        solveAtOnceM.setEnabled(false);
        solveAtOnceM.addActionListener(this);
        
        solveM = new JMenuItem();
        solveM.setText("Solve...");
        solveM.setEnabled(false);
        solveM.addActionListener(this);
        
        hintM = new JMenuItem();
        hintM.setText("Hint");
        hintM.setEnabled(false);
        hintM.addActionListener(this);
        
        aboutM = new JMenuItem();
        aboutM.setText("About...");
        aboutM.addActionListener(this);

        fileMenu.add(newMenu);
        fileMenu.add(openM);
        fileMenu.add(openPresetM);
        fileMenu.addSeparator();
        fileMenu.add(restartM);
        fileMenu.add(closeM);
        fileMenu.addSeparator();
        fileMenu.add(saveAsM);
        fileMenu.addSeparator();
        fileMenu.add(printM);
        fileMenu.addSeparator();
        fileMenu.add(exitM);
        viewMenu.add(showSolvePanelM);
        viewMenu.add(showTraceM);
        viewMenu.add(showTTM);
        viewMenu.addSeparator();
        viewMenu.add(clearMarksM);
        viewMenu.addSeparator();
        viewMenu.add(loadThemeM);
        viewMenu.add(loadDefaultThemeM);
        viewMenu.add(saveThemeM);
        viewMenu.add(saveDefaultM);
        viewMenu.addSeparator();
        viewMenu.add(prefsM);
        solverMenu.add(undoM);
        solverMenu.add(redoM);
        solverMenu.addSeparator();
        solverMenu.add(hintM);
        solverMenu.addSeparator();
        solverMenu.add(solveAtOnceM);
        solverMenu.add(solveM);
        aboutMenu.add(aboutM);
	}
	
	/**
	 * Initializes all visual components, adds Listeners,
	 * creates Menus, etc.
	 * This method is called by the Constructor only.
	 *
	 */
	private void initComponents(){
		//this.setPreferredSize(new Dimension(350,400));
		this.setPreferredSize(new Dimension(390, 440));
		label = new JLabel(".:: MUSoSu ::.");
		label.setFont(new Font("Courier", Font.BOLD, 24));
    	label.setForeground(Color.orange);
		
        titlePanel = new JPanel();
    	titlePanel.setLayout(new BorderLayout());
    	titlePanel.setBackground(Color.DARK_GRAY);
    	titlePanel.add(label, BorderLayout.EAST);

    	newCustomPanel = new JPanel();
    	newCustomPanel.setLayout(new BorderLayout(1, 1));
    	
    	JPanel butPan = new JPanel();
    	getStr = new JButton("Get String");
    	getStr.setBackground(Color.WHITE);
    	getStr.addActionListener(this);
    	getStr.setMargin(new Insets(1,1,1,1));
    	save = new JButton("Save"); 
    	save.setBackground(Color.WHITE);
    	save.addActionListener(this);
    	save.setMargin(new Insets(1,1,1,1));
    	savenopen = new JButton("Save and Play"); savenopen.setBackground(Color.WHITE);
    	savenopen.addActionListener(this);
    	savenopen.setMargin(new Insets(1,1,1,1));
    	cancel = new JButton("Cancel"); cancel.setBackground(Color.WHITE);
    	cancel.addActionListener(this);
    	cancel.setMargin(new Insets(1,1,1,1));
    	butPan.add(getStr);
    	butPan.add(save);
    	butPan.add(savenopen); 
    	butPan.add(cancel);
    	butPan.setVisible(true);
    	newCustomPanel.add(butPan, BorderLayout.EAST);
    	newCustomPanel.setVisible(false);
    	statusPanel = new MStatusPanel(); // I know it's duplicated but GamePanel() needs it
    	
    	middle = new JPanel();
		middle.setLayout(new BorderLayout());
    	panel = new GamePanel(statusPanel);
		panel.setVisible(false);
		msp = new MSolvePanel(panel);   // for the solver panel
										// more initialisation in showSolverPanel()
		msp.setVisible(false);
		middle.add(msp, BorderLayout.EAST);
		middle.add(panel, BorderLayout.CENTER);
		
		//
		statusPanel = new MStatusPanel();
    	statusPanel.setVisible(true);
    	
    	southPanel = new JPanel();
    	southPanel.setLayout(new BorderLayout());
    	southPanel.add(newCustomPanel, BorderLayout.NORTH);
    	southPanel.add(statusPanel, BorderLayout.CENTER);
    	southPanel.setVisible(true);    	
		//
		
    	superPanel = new JPanel();
        superPanel.setLayout(new BorderLayout());
        superPanel.add(titlePanel, BorderLayout.NORTH);
        superPanel.add(middle, BorderLayout.CENTER);
        superPanel.add(southPanel, BorderLayout.SOUTH);       
        getContentPane().add(superPanel);
 }
	
	/**
	 * Method to load on preloadedNames and preloaded
	 * the hardcoded games
	 */
	private void preloadGames(){
		preloaded = new Vector<String>();
		preloadedNames = new Vector<String>();
		String n = new String();
		String s = new String();
		n = "Very Easy - 1";
		s = "870000015903040706060108020006405200040000050005901600020304090108090504450000062";
		preloadedNames.add(n);
		preloaded.add(s);
		
		n = "Medium - 1";
		s = "040203000050000093008000700700304009000000000900506008004000900260000040000109080";
		preloadedNames.add(n);
		preloaded.add(s);
		
		n = "Medium - 2";
		s = "00300040000030800079000001801020306000004000002060507038000002700090700000200090";
		preloadedNames.add(n);
		preloaded.add(s);
		
		n = "Hard - 1";
		s = "000050070207800600010040050000000030406000105020000000040030010003006908060080000";
		preloadedNames.add(n);
		preloaded.add(s);
		
		n = "Very Hard - 1";
		s = "00090400000406020003000008020030500609000001010060700805000009000102030000080900";
		preloadedNames.add(n);
		preloaded.add(s);
		
		n = "Very Hard - 2";
		s = "060000010500406003000503000021000650000000000053000840000102000200705009090000080";
		preloadedNames.add(n);
		preloaded.add(s);
		
		n = "Very Hard - 3";
		s = "500009004002000800100403006907000400000080000001000203700604009005000700400100002";
		preloadedNames.add(n);
		preloaded.add(s);
	}
	
	private void showSolverPanel(){
		msp.setSudoku(game);
		msp.setVisible(true);
		if(!((this.getState() & JFrame.MAXIMIZED_BOTH) == JFrame.MAXIMIZED_BOTH)){
			setSize(new Dimension(this.getSize().width + 200, this.getSize().height));
		}
		this.validateTree();
	}
	
	private void hideSolverPanel(){
		msp.setVisible(false);
		if(!((this.getState() & JFrame.MAXIMIZED_BOTH) == JFrame.MAXIMIZED_BOTH)){
			setSize(new Dimension(this.getSize().width - 200, this.getSize().height));
		}
	}
	
	private void getStrEvent(){
		String str = game.getString();
		GetStrDialog getStrDlg = new GetStrDialog(this, str);
		getStrDlg.setVisible(true);
	}
	
}
