/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.colorchooser.AbstractColorChooserPanel;

/**
 * <p>Class <code>ColorDialog</code> is just a dialog for selecting color
 * using the JColorChooser component</p>
 * @author Visvardis Marios
 */
public class ColorDialog extends JDialog implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	private Color color;
	private JColorChooser cch;
	
	
	public ColorDialog(JFrame parent, String title, Color col){
		super(parent, title, true);
		getContentPane().setLayout(new BorderLayout());
		cch = new JColorChooser();
		
		// There must be a cleaner way to do this
		AbstractColorChooserPanel[] accp;
		accp = cch.getChooserPanels();
		cch.removeChooserPanel(accp[2]);
		cch.removeChooserPanel(accp[0]);
		// TODO replace this code block
		
		cch.setColor(col); // set default color
		JButton button = new JButton();
		button.setText("OK");
		button.addActionListener(this);
		getContentPane().add(cch, BorderLayout.CENTER);
		getContentPane().add(button, BorderLayout.SOUTH);
		pack();
		setVisible(true);
	}
	
	public Color getColor(){
		return color;
	}

	public void actionPerformed(ActionEvent e){
		color = cch.getColor();
		setVisible(false);
		dispose();
	}
}
