/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import sudoku.Sudoku;
import sudokuGenerator.FillFirst;
import sudokuGenerator.GenerateMethod;

@SuppressWarnings("serial")
public class GenerateDialog extends JDialog implements ActionListener{
	private JButton button;
	private JButton cancel;
	private GenerateMethod generator;
	private JSlider slider;
	private Sudoku game;
	
	public GenerateDialog(JFrame parent, String title){
		super(parent, title, true);
		if(parent != null){
			  Dimension parentSize = parent.getSize(); // Parent size
			  Point p = parent.getLocation();          // Parent position
			  setLocation(p.x+parentSize.width/4,p.y+parentSize.height/4);
		}
		
		getContentPane().setLayout(new GridLayout(2,1,1,1));		
		
		JPanel difPanel = new JPanel();
		
		JLabel label = new JLabel(" Select difficulty:");
		
		slider = new JSlider();
		slider.setMaximum(10);
		slider.setMinimum(1);
		slider.setPaintTrack(true);
		slider.setPaintLabels(true);
		slider.setMinorTickSpacing(1);
		slider.setSnapToTicks(true);
		slider.setValue(5);
		slider.setPaintTicks(true);
		slider.setVisible(true);

		difPanel.setLayout(new BorderLayout());
		difPanel.setBorder(BorderFactory.createEtchedBorder());
		difPanel.add(label, BorderLayout.NORTH);
		difPanel.add(slider, BorderLayout.CENTER);
		
		getContentPane().add(difPanel);
		
		JPanel submitPanel = new JPanel();
		submitPanel.setLayout(new BorderLayout());
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBackground(Color.WHITE);
		
		button = new JButton();
		button.setBackground(Color.WHITE);
		button.setText("Generate");
		button.addActionListener(this);
		
		cancel = new JButton();
		cancel.setBackground(Color.WHITE);
		cancel.setText("Cancel");
		cancel.addActionListener(this);
		
		buttonPanel.setLayout(new BorderLayout());
		buttonPanel.add(button, BorderLayout.EAST);
		buttonPanel.add(cancel, BorderLayout.WEST);
		submitPanel.add(buttonPanel, BorderLayout.SOUTH);
		getContentPane().add(submitPanel);
		

		
		setResizable(false);
		pack();
		
	}
	
	public void actionPerformed(ActionEvent evt){
		if(evt.getSource() == button){
			generator = new FillFirst(slider.getValue() * 10);
//			jpb.setVisible(true);
			game = generator.generate();
			 // TODO argument from dialog
		}else{
			generator = null;
			game = null;
		}
		
		setVisible(false);
		dispose();
	}
	
	public GenerateMethod getGenerator(){
		return this.generator;
	}
	
	public Sudoku getGame(){
		return game;
	}
}
