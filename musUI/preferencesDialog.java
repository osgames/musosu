/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * <p><code>preferencesDialog</code> is a JDialog child, that is used
 * in order to customize the appearence and the behaviour of the application.
 * It provides a tabbedPane user interface and communicates with the main frame 
 * using a <code>Settings</code> object.</p>
 * <p>The first tab has color settings and contains some <code>ColorSelectPanel
 * </code> objects, and the second tab has various game settings.
 * @author Visvardis Marios
 * @version 0.2.0
 * @see ColorSelectPanel
 * TODO change customs colors GridLayout to GridBagLayout
 */
@SuppressWarnings("serial")
public class preferencesDialog extends JDialog{
	private JPanel customColorsPanel;
	private JPanel mainPanel;
	private JPanel gameSettingsPanel;
	private JPanel butPanPan;
	private JCheckBox chPossibles;
	private JCheckBox chTrace;
	private ColorSelectPanel csp1, csp2, csp3, csp4, csp5;
	private ColorSelectPanel csp6, csp7, csp8, csp9;
	private JFrame parent;
	private Settings tmp;
	private Settings original;
	
	public preferencesDialog(JFrame parent, Settings settings){
		super(parent, "Preferences", true);
		
		if(parent != null){
			  Dimension parentSize = parent.getSize(); // Parent size
			  Point p = parent.getLocation();          // Parent position
			  setLocation(p.x+parentSize.width,p.y);
		}
		
		this.parent = parent;
		this.original = settings;
		tmp = new Settings();
		initcustomColorsPanel();
		initButtons();
		initGameSettingsPanel();
		initMainPanel();
		
		getContentPane().add(mainPanel);
		pack();
	}
	
	private void initMainPanel(){
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.insertTab("Select Colors", null, customColorsPanel, "Select custom colors", 0);
		tabbedPane.insertTab("GameSettings", null, gameSettingsPanel, "Various game settings", 1);
		mainPanel.add(butPanPan, BorderLayout.SOUTH);
		mainPanel.add(tabbedPane, BorderLayout.CENTER);
	}
	
	private void initGameSettingsPanel(){
		gameSettingsPanel = new JPanel();
		gameSettingsPanel.setLayout(new GridLayout(7, 1, 1, 1));
		gameSettingsPanel.setBackground(Color.WHITE);
		JPanel checkPanel = new JPanel();
		checkPanel.setLayout(new GridLayout(2, 1, 1, 1));
		
		chTrace = new JCheckBox();
		chTrace.setText("Highlight current line, column, box (trace)");
		chTrace.setSelected(original.getShowTrace());
		chTrace.setBackground(Color.WHITE);

		chPossibles = new JCheckBox();
		chPossibles.setText("Show possible values as tooltip");
		chPossibles.setSelected(original.getShowToolTip());
		chPossibles.setBackground(Color.WHITE);
		 
		checkPanel.add(chTrace);
		checkPanel.add(chPossibles);
		checkPanel.setBackground(Color.WHITE);
		gameSettingsPanel.add(checkPanel);
	}
	
	public Settings getSettings(){
		return original;
	}
	
	private void setParentColors(){
		tmp.setHighlightColor(csp1.getColor());
		tmp.setButtonBackColor(csp2.getColor());
		tmp.setPanelBackColor(csp3.getColor());
		tmp.setErrorBackColor(csp4.getColor());
		tmp.setFixedBackColor(csp5.getColor());
		tmp.setMarkColorA(csp6.getColor());
		tmp.setMarkColorB(csp7.getColor());
		tmp.setMarkColorC(csp8.getColor());
		tmp.setMarkColorD(csp9.getColor());
	}
	
	private void setParentGameSettings(){
		tmp.setShowToolTip(chPossibles.isSelected());
		tmp.setShowTrace(chTrace.isSelected());
	}
	
	private void initButtons(){
		JPanel butPan = new JPanel();
		JButton ok = new JButton();
		ok.setText("OK");
		ok.setBackground(Color.WHITE);
		ok.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				okEvent(evt);
			}
		});
		JButton cancel = new JButton();
		cancel.setText("Cancel");
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				cancelEvent(evt);
			}
		});
		butPanPan = new JPanel();
		butPanPan.setLayout(new BorderLayout());
		
		butPan.add(cancel);
		butPan.add(ok);
		butPanPan.add(butPan, BorderLayout.EAST);
	}
	
	private void initcustomColorsPanel(){
		customColorsPanel = new JPanel();
		customColorsPanel.setLayout(new GridLayout(9,1));
		
		csp1 = new ColorSelectPanel(parent, original.getHighlightColor(), "Color for trace \n" +
				                            "function");
		csp2 = new ColorSelectPanel(parent, original.getButtonBackColor(), "Cell Background \n" +
				                            "color");
		csp3 = new ColorSelectPanel(parent, original.getPanelBackColor(),"Background color \n" +
				                            "of the game board");
		csp4 = new ColorSelectPanel(parent, original.getErrorBackColor(),"Cell Background \n" +
				                            "color Error values");
		csp5 = new ColorSelectPanel(parent, original.getFixedBackColor(),"Background color \n" +
				                            "for preset values");
		csp6 = new ColorSelectPanel(parent, original.getMarkColorA(), "Mark color A");
		csp7 = new ColorSelectPanel(parent, original.getMarkColorB(), "Mark color B");
		csp8 = new ColorSelectPanel(parent, original.getMarkColorC(), "Mark color C");
		csp9 = new ColorSelectPanel(parent, original.getMarkColorD(), "Mark color D");
		
		customColorsPanel.add(csp1); customColorsPanel.add(csp2);
		customColorsPanel.add(csp3); customColorsPanel.add(csp4);
		customColorsPanel.add(csp5);
		customColorsPanel.add(csp6); customColorsPanel.add(csp7);
		customColorsPanel.add(csp8); customColorsPanel.add(csp9);
	}
	
	public void okEvent(ActionEvent e){
		setParentColors();
		setParentGameSettings();
		original = tmp;
		setVisible(false);
		dispose();
	}
	
	public void cancelEvent(ActionEvent e){
		setVisible(false);
		dispose();
	}
}
