/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package musUI;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;

import javax.swing.JPanel;

/**
 * <code>ImagePanel</code> Simple JPanel child that displays
 * an image.
 * @author Visvardis Marios
 */
@SuppressWarnings("serial")
public class ImagePanel extends JPanel{
	private Image img;
	
	public ImagePanel(Image image) throws IOException
	{
          this.img = image;
	}
	
	public void paint(Graphics g)
	{
		if( img != null){
			g.drawImage(img,0,0, this);
		}
	}
}
