/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudoku;

import java.util.Vector;

public class Board {
	private Cell[] Cells;
	private int whereCantIAdd;
	private int whyCantIAdd;
	private int completed;
	private int[] numbers;
	
	public Box[] Line;
	public Box[] Column;
	public Box[] Kouti;
	
	public int getRatio(){
		int ratio = completed / 81;
		return ratio;
	}
	
	public int getCompleted(){
		return completed;
	}
	
	public int get(int position){
		return Cells[position].getValue();
	}
	
	public int getKartes(int x, int y){
		return Line[x].get(y);
	}
	
	public int getKoutia(int k, int l){
		return Kouti[k].get(l);
	}
	
	public boolean isFixed(int p){
		return Cells[p].isFixed();
	}
	
	public void set(int num, int pos){
		Cells[pos].setFixedValue(num);
		completed++;
		numbers[num]++; numbers[0]--;
	}
	
	public void add(int num, int pos){
		int old = Cells[pos].getValue();
		if(num != 0 && old != num)
			completed++;
		Cells[pos].putValue(num);
		numbers[num]++; numbers[old]--;
	}
	
	public void remove(int pos){
		int num = Cells[pos].getValue();
		if(num != 0 && (!Cells[pos].isFixed())){
			Cells[pos].removeValue();
			completed--;
		}
		numbers[num]--;
	}
	
	public int getNumberInstances(int num){
		return numbers[num];
	}
	
	public void everythingIsPossible(){
		for(int i = 0; i < 81; i++)
			if(!Cells[i].isFixed() && Cells[i].getValue() == 0)
				Cells[i].everythingIsPossible();
	}
	
	public void setImpossible(int num, int position){
		Cells[position].setImpossible(num);
	}
	
	public Vector<Integer> getPossibles(int position){
		return Cells[position].getPossibles();
	}
	
	public void setPossibles(int index, Vector<Integer> possibles){
		Cells[index].setPossibles(possibles);
	}
	
	public boolean canIAdd(int num, int pos){
		int[] kart = new int[2];
		int[] kouti = new int[2];
		int posi;
		kart = Convert.indexToKartes(pos);
		kouti = Convert.indexToKoutia(pos);
		if((posi = Line[kart[0]].exists(num)) != -1){
			whereCantIAdd = 1;
			whyCantIAdd = posi;
		}
		if((posi = Column[kart[1]].exists(num)) != -1){
			whereCantIAdd = 2;
			whyCantIAdd = posi;
		}
		if((posi = Kouti[kouti[0]].exists(num)) != -1){
			whereCantIAdd = 3;
			whyCantIAdd = posi;
		}
		
		return Cells[pos].canIPut(num);
	}
	
	public int[] whyICant(){
		int[] ret = new int[2];
		ret[0] = whereCantIAdd;
		ret[1] = whyCantIAdd;
		return ret;
	}
	
	public Board(){
		Cells = new Cell[81];
		
		Line = new Box[9];
		Column = new Box[9];
		Kouti = new Box[9];
		
		numbers = new int[10];
		for(int i = 0; i < 10; i++)
			numbers[i] = 0;
		
		completed = 0;

		for(int i = 0; i < 81; i++){
			Cells[i] = new Cell();
		}
		
		for(int i = 0; i < 9; i++){
			Line[i] = new Box(i, 1);
			Column[i] = new Box(i, 2);
			Kouti[i] = new Box(i, 3);
		}
	}
	
	public class Box {
		private Cell[] Cont;
		private boolean full;
		private int count;
	
		public boolean checkFull(){
			int i, num;
			int[] nums = new int[9];
			
			for(i = 0; i < 9; i++){
				nums[i] = i + 1;
			}
			for(i = 0; i < 9; i++){
				num = Cont[i].getValue();
				if(num == 0){
					return false;
				}
				nums[num - 1] = 0;
				
			}
			for(i = 0; i < 9; i++)
				if(nums[i] != 0)
					return false;
			return true;
		}
		
		public void add(int num, int pos){
			Cont[pos].putValue(num);
			if(num != 0) count++;
			if(count == 9) full = true;
		}
		
		public boolean canIAdd(int num, int pos){
			return Cont[pos].canIPut(num);
		}
		
		public boolean isFull(){
			return full;
		}
		
		public int getFilled(){
			return this.count;
		}
		
		public int get(int pos){
			return Cont[pos].getValue();
		}
		
		public int exists(int num){
			int i, pos = -1;
			for(i = 0; i < 9; i++){
				if(Cont[i].getValue() == num){
					pos = i;
				}
			}
			return pos;
		}
		
		public void setImpossible(int num, int position){
			Cont[position].setImpossible(num);
		}

		public Vector<Integer> getPossibles(int position){
			return Cont[position].getPossibles();
		}
		
		public Box(int ind, int mode){
			int first;
			int plus;
			
			full = false;
			count = 0;	
			
			Cont = new Cell[9];
			
			switch(mode){
			case 1:   // for lines
				first = ind * 9;
				for(int i = 0; i < 9; i++){
				Cont[i] = Cells[(first + i)];
				}
				break;
			case 2:   // for columns
				first = ind;
				plus = 9;
				for(int i = 0; i < 9; i++){
					Cont[i] = Cells[first + i * plus];
				}
				break;
			case 3:   // for koutia
				first = Convert.koutiaToIndex(ind, 0);
				for(int j = 0; j < 3; j++){
					plus = 1;
					for(int i = 0; i < 3; i++){
						Cont[j * 3 + i] = Cells[first + plus * i];
					}
					first = first + 9;
				}
				break;
			}
		}

	}
}
