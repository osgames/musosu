/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudoku;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;

import javax.swing.event.EventListenerList;

import sudokuSolver.SolvedSquare;

public class Sudoku {
	private String fileName;
	private Board active;
	private boolean persistantPossibles;
	private EventListenerList listenerList;
	private SudokuEvent sudokuEvent;
	private double difficulty;
	
	public Sudoku(){
		init();
	}
	
	/**
	 * Constructor for Copying Sudoku objects
	 */ 
	public Sudoku(Sudoku source){
		init();
		setFileName(source.fileName);
		int num;
		for(int i = 0; i < 81; i++){
			num = source.get(i);
			if(num != 0){
				if(source.isFixed(i)){
					active.set(num, i);
				}else{
					active.add(num, i);
				}
			}
		}
		refreshPossibleMap();
	}
	
	public Sudoku(String fileName){
		init();
		setFileName(fileName);
		
		loadFromFile();
		refreshPossibleMap();
	}
	
	public Sudoku(String s, int dummy){
		init();
		setFileName("NoFile");
		loadFromString(s);
		refreshPossibleMap();
	}
	
	public void init(){
		persistantPossibles = false;
		active = new Board();
		listenerList = new EventListenerList();
		sudokuEvent = null;
	}
	
	public int getRatio(){return active.getRatio();}
	
	public int getCompleted(){return active.getCompleted();}
	
	public boolean checkFull(){
		for(int i = 0; i < 9; i++){
			if(!active.Line[i].checkFull())
				return false;
			if(!active.Column[i].checkFull())
				return false;
			if(!active.Kouti[i].checkFull())
				return false;
		}
		return true;
	}
	
	private void loadFromString(String s){
		int i, d = 0;
		char c;
		
		for(i = 0; i < s.length(); i++){
			c = s.charAt(i);
			if(Character.isDigit(c)){
				if(c != '0'){
					set(Integer.valueOf(String.valueOf(c)), d);
				}
			}
			d++;
		}
	}
	
	private void loadFromFile(){
		String line;
		StringBuffer contents = new StringBuffer();
		BufferedReader input = null;
		File aFile = new File(fileName);
		try{
			FileReader fr = new FileReader(aFile);
			input = new BufferedReader(fr);
			while((line = input.readLine()) != null){
				contents.append(line);
				contents.append(System.getProperty("line.separator"));
			}
		}
		catch(FileNotFoundException ex){
			ex.printStackTrace();
		}
		catch(IOException ex){
			ex.printStackTrace();
		}
		finally{
			try{
				if(input != null){
					input.close();
				}
			}
			catch(IOException ex){
				ex.printStackTrace();
			}
		}
		line = contents.toString();
		
		char c;
		int d = 0;
		for(int i = 0; i < line.length(); i++){
			c = line.charAt(i);
			if(Character.isDigit(c)){
				if(c != '0'){
					set(Integer.valueOf(String.valueOf(c)), d);
				}
				d++;
			}
		}
	}
	
	
	public void saveToFile(String fname){
		int index = 0;
		PrintStream MyOutput = null;
		try{
			MyOutput = new PrintStream(new FileOutputStream(fname));
		}
		catch (IOException e){
			System.out.println("OOps");
		}
		for(int i = 0; i < 9; i++){
			for(int j = 0; j < 9; j++){
				MyOutput.print(String.valueOf(active.get(index)) + " ");
				index++;
			}
			MyOutput.print("\n");
		}
		
		MyOutput.close();
	}

	public void add(int num, int position){
		active.add(num, position);
		refreshPossibleMap();
		this.fireSudokuEvent(SudokuEvent.SUDOKU_CHANGED);
	}
	
	public void set(int num, int position){
		active.set(num, position);
	}
	
	public void remove(int position){
		active.remove(position);
		refreshPossibleMap();
		this.fireSudokuEvent(SudokuEvent.SUDOKU_CHANGED);
	}
	
	public void addFromSQV(Vector<SolvedSquare> sqv){
		int i;
		for(i = 0; i < sqv.size(); i++)
			this.add(sqv.get(i).getValue(), sqv.get(i).getIndex());
	}
	
	public int getNumberInstances(int num){
		return active.getNumberInstances(num);
	}
	
//	TODO this method will improve performance of most solving methods
//	public int[] getNumbersOrderedByFrequency(){	
//	}
	
	public int existsInLine(int number, int line){
		return active.Line[line].exists(number);
	}
	
	public int existsInColumn(int number, int column){
		return active.Column[column].exists(number);
	}
	
	public int existsInBox(int number, int box){
		return active.Kouti[box].exists(number);
	}
	
	public boolean isLineFull(int line){return active.Line[line].isFull();}
	public boolean isColumnFull(int column){return active.Column[column].isFull();}
	public boolean isBoxFull(int box){return active.Kouti[box].isFull();}
	
	public int getLineNumbersFilled(int line){
		return active.Line[line].getFilled();
	}
	public int getColumnNumbersFilled(int column){
		return active.Column[column].getFilled();
	}
	public int getBoxNumbersFilled(int box){
		return active.Kouti[box].getFilled();
	}
	
	
	public void refreshPossibleMap(){
		int i, j, num;
		
		if(!persistantPossibles){
			active.everythingIsPossible();
		}
		for(num = 1; num < 10; num++){
			for(i = 0; i < 9; i++){
				if(active.Line[i].exists(num) != -1){
					for(j = 0; j < 9; j++){
						active.Line[i].setImpossible(num, j);
					}
				}
				if(active.Column[i].exists(num) != -1){
					for(j = 0; j < 9; j++){
						active.Column[i].setImpossible(num, j);
					}
				}
				if(active.Kouti[i].exists(num) != -1){
					for(j = 0; j < 9; j++){
						active.Kouti[i].setImpossible(num, j);
					}
				}
			}
		}
	}
	
	public void setImpossible(int number, int position){
		active.setImpossible(number, position);
	}
	
	public Vector<Integer> getPossibles(int position){
		return active.getPossibles(position);
	}
	
	public void setPossibles(int index, Vector<Integer> possibles){
		active.setPossibles(index, possibles);
	}
	
	public boolean canIAdd(int num, int position){
		return active.canIAdd(num, position);
	}
	
	public int[] whyICant(){
		return active.whyICant();
	}
	
	public int get(int pos){
		return active.get(pos);
	}
	
	public boolean isFixed(int pos){
		return active.isFixed(pos);
	}
	
	public void setFileName(String fn){
		fileName = new String(fn);
	}


// METHODS FOR EVENT GENERATION AND SETUP
	public void addSudokuListener(SudokuListener l) {
		listenerList.add(SudokuListener.class, l);
	}

	public void removeSudokuListener(SudokuListener l) {
		listenerList.remove(SudokuListener.class, l);
	}

	public void fireSudokuEvent(int type) {
		Object[] listeners = listenerList.getListenerList();
	
		for (int i = listeners.length-2; i>=0; i-=2) {
			if (listeners[i] == SudokuListener.class) {
				if (sudokuEvent == null)
					sudokuEvent = new SudokuEvent(this, type);
				if(sudokuEvent.getType() == SudokuEvent.SUDOKU_CHANGED)
					((SudokuListener)listeners[i+1]).sudokuChanged(sudokuEvent);
			}
		}	
	}
	
	public void setPersistantPossibles(boolean state){
		persistantPossibles = state;
	}
	
	public boolean getPersistantes(){
		return persistantPossibles;
	}
	
	public void setDifficulty(double dif){
		this.difficulty = dif;
	}
	
	public double getDifficulty(){
		return this.difficulty;
	}
	
	public String getString(){
		String ret = new String();
		for(int i = 0; i < 81; i++)
			ret += String.valueOf(get(i));
		return ret;
	}
}