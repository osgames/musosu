/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudoku;

import java.util.EventObject;

/**
 * <code>SudokuEvent</code>
 * <p>SudokuEvent object holds the information for events on 
 * a Sudoku class</p>
 * @author Visvardis Marios
 * @see SudokuListener
 * @see Sudoku
 * @see EventObject
 */
public class SudokuEvent extends EventObject{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int SUDOKU_CHANGED = 1;
	private int type;
	
	public SudokuEvent(Object source, int type){
		super(source);
		this.type = type;
	}
	
	 public int getType(){
		 return type;
	 }
}
