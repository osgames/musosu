/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudoku;

public class Convert {
	public static final int LINE_MODE = 0;
	public static final int COLUMN_MODE = 1;
	public static final int BOX_MODE = 2;
	
	public static int[] koutiaToKartes(int k, int l){
		int[] ret = new int[2];
		int t1, t2;
		
		t1 = k / 3;
		t2 = l / 3;
		ret[0] = 3 * t1 + t2;
		t1 = k % 3;
		t2 = l % 3;
		ret[1] = 3 * t1 + t2;
		
		return ret;
	}
	
	/**
	 *  KartesToKoutia
	 *  Converts coordinates from cartesian(x,y) to box coordinates(k,l)
	 *  
	 *  e.g.             |(1,1) (1,2) (1,3)| in          |(1,1) (1,2) (1,3)|
	 *  for box 1:       |(1,4) (1,5) (1,6)| cartesian:  |(2,1) (2,2) (2,3)|          
	 *                   |(1,7) (1,8) (1,9)|             |(3,1) (3,2) (3,3)| etc.
	 */
	public static int[] kartesToKoutia(int x, int y){
		int t1, t2;
		int[] tmp = new int[2];
		
		t1 = x / 3;
		t2 = y / 3;
		tmp[0] = 3 * t1 + t2;
		t1 = x % 3;
		t2 = y % 3;
		tmp[1] = t1 * 3 + t2;
		
		return tmp;
	}

	public static int kartesToIndex(int x, int y){
		return (x * 9) + y;
	}
	
	public static int koutiaToIndex(int k, int l){
		int x, y;
		int[] tmp = new int[2];
		tmp = koutiaToKartes(k, l);
		x = tmp[0]; y = tmp[1];
		return kartesToIndex(x, y);
	}
	
	public static int[] indexToKartes(int index){
		int[] ret = new int[2];
		ret[0] = index / 9;
		ret[1] = index % 9;
		return ret;
	}
	
	public static int[] indexToKoutia(int index){
		int[] kar = new int[2];
		int[] ret = new int[2];
		kar = indexToKartes(index);
		ret = kartesToKoutia(kar[0], kar[1]);
		return ret;
	}
	
	public static int[] getRowsOnBox(int boxNumber){
		int[] rows = new int[3];
		int[] kartes = new int[2];
		kartes = koutiaToKartes(boxNumber, 0);
		for(int i = 0; i < 3; i++)
			rows[i] = kartes[0] + i;
		return rows;
	}
	
	public static int[] getColumnsOnBox(int boxNumber){
		int[] columns = new int[3];
		int[] kartes = koutiaToKartes(boxNumber, 0);
		for(int i = 0; i < 3; i++){
			columns[i] = kartes[1] + i;
		}
		return columns;
	}
	
	public static int[] getBoxesOnLine(int lineNumber){
		int[] boxes = new int[3];
		int[] kout = Convert.kartesToKoutia(lineNumber, 0);
		for(int i = 0; i < 3; i++)
			boxes[i] = kout[0] + i;
		return boxes; 
	}
	
	public static int[] getBoxesOnColumn(int columnNumber){
		int[] boxes = new int[3];
		int[] kout = Convert.kartesToKoutia(0, columnNumber);
		for(int i = 0; i < 3; i++)
			boxes[i] = kout[0] + 3 * i;
		return boxes;
	}
	
	public static int translateToIndex(int mode, int group, int item){
		int index = -1;
		switch(mode){
		case LINE_MODE:
			index = kartesToIndex(group, item);
			break;
		case COLUMN_MODE:
			index = kartesToIndex(item, group);
			break;
		case BOX_MODE:
			index = koutiaToIndex(group, item);
			break;
		}
		return index;
	}	
}
