/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudoku;

import java.util.EventListener;

/**
 * <p>MUSoSu project</p>
 * <code>SudokuListener</code>
 * <p>SudokuListener interface. Contains only sudokuChange method.</p>
 * @author Visvardis Marios
 * @see SudokuEvent
 * @see Sudoku
 * @see EventListener
 */
public interface SudokuListener extends EventListener{
	/**
	 * <code>sudokuChanged</code>
	 * <p>Method to handle a change (addition or removal) of 
	 * a value of the game board.</p> 
	 * @param e
	 */
	public void sudokuChanged(SudokuEvent e);
}

