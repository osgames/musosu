/*
 * This file is part of MUSoSu.
 *
 * MUSoSu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * MUSoSu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MUSoSu.  If not, see <http://www.gnu.org/licenses/>.
 */

package sudoku;

import java.util.Vector;

public class Cell {
	private Vector<Integer> possibleValues;
	private int value;
	private boolean fixed;
	
	public Cell(){
		possibleValues = new Vector<Integer>();
		everythingIsPossible();
		value = 0;
		fixed = false;
	}
	
	public void everythingIsPossible(){
		int i;
		possibleValues.clear();
		for(i = 1; i < 10; i++){
			possibleValues.add(new Integer(i));
		}
	}
	
	public void setFixedValue(int value){
		this.value = value;
		fixed = true;
		possibleValues.clear();
	}
	
	public void putValue(int value){
		this.value = value;
		possibleValues.clear();
	}
	
	public int getValue(){
		return value;
	}
	
	public void removeValue(){
		value = 0;
		everythingIsPossible();
	}
	
	public boolean canIPut(int num){
		for(int i = 0; i < possibleValues.size(); i++)
			if(possibleValues.get(i).intValue() == num)
				return true;
		return false;
	}
	
	public void setImpossible(int num){
		if(possibleValues.contains(new Integer(num))){
			possibleValues.removeElement(new Integer(num));
		}
	}
	
	public Vector<Integer> getPossibles(){
		if(isFixed() && this.value != 0){
			possibleValues.clear();
		}
		return possibleValues;
	}
	
	public void setPossibles(Vector<Integer> p){
		Vector<Integer> newPossibles = new Vector<Integer>();
		for(Integer i: p){
			newPossibles.add(i);
		}
		possibleValues = newPossibles;
	}
	
	public boolean isFixed(){
		return fixed;
	}
}
